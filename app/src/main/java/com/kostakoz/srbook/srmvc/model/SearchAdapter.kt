package com.kostakoz.srbook.srmvc.model

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Handler
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.BackgroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.kostakoz.srbook.ChaptersActivity
import com.kostakoz.srbook.FullscreenActivity
import com.kostakoz.srbook.R
import com.kostakoz.srbook.SearchActivity
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.dataclasses.Pages


private const val TAG = "customAdapter"

class SearchAdapter(
    private val pagesList: ArrayList<Pages>,
    private val query: String
) :
    RecyclerView.Adapter<SearchAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {

        var textViewChapter: TextView =
            itemView.findViewById<View>(R.id.textViewChapter) as TextView
        var textViewText: TextView = itemView.findViewById<View>(R.id.textViewText) as TextView
        var textViewPage: TextView = itemView.findViewById<View>(R.id.textViewPage) as TextView
        var cardView: CardView = itemView.findViewById(R.id.cardView) as CardView
        var view = itemView

    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cards_layout_search, parent, false)
//        view.setBackgroundColor(Color.TRANSPARENT)
        return MyViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        when (MyPref.getThemeMode()) {
            0 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.colorBackground))
            1 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.cardviewBackground2))
            2 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.cardviewBackground))
            else -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.colorBackground))
        }

        val pages = pagesList[listPosition]
        val chapters = DataFromFile2.dataBaseAdapter.getOneChapters(pages.id_data)!!

        holder.textViewChapter.text = chapters.title
        val spannable = HtmlToSpannable().getSpannableFromHtml(pages.pages)
        val string = spannable.toString()
        val start = string.indexOf(query)
        val end = start + query.length
        val newStart = if (start - 50 < 0) start else start - 50
        val newEnd = if (end + 50 > string.length) end else end + 50
        val text = string.substring(newStart, newEnd)
        val spannableStringBuilder = SpannableStringBuilder(text)
        spannableStringBuilder.setSpan(
            BackgroundColorSpan(Color.rgb(220, 231, 117)), text.indexOf(query)
            , text.indexOf(query) + query.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        holder.textViewText.text = spannableStringBuilder

        val str = (pages.page + 1).toString() + " стр"
        holder.textViewPage.text = str



        holder.cardView.setOnClickListener {
            Handler().postDelayed({
                // This method will be executed once the timer is over
                // Start your app main activity
                DataFromFile2.dataBaseAdapter.updateCurrentPage(pages.page)
                val context = holder.cardView.context
                val activity = context as SearchActivity
                activity.startActivity(
                    Intent(
                        activity,
                        FullscreenActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                )
                activity.finish()

            }, 1)
        }
    }

    override fun getItemCount(): Int {
        return pagesList.size
    }

}