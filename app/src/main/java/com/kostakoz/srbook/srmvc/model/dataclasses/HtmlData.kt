package com.kostakoz.srbook.srmvc.model.dataclasses

import android.text.SpannableStringBuilder

data class HtmlData(
    val id: Int,
    val spannable: SpannableStringBuilder
)