package com.kostakoz.srbook.srmvc.model.dataclasses

import android.graphics.Color

data class Selected(
    var id: Int,
    var favorite: Int,
    var id_data: Int,
    var start: Int,
    var end: Int,
    var startMain: Int,
    var endMain: Int,
    var page: Int,
    var color: Int = Color.GRAY,
    var text: String,
    var description: String
)