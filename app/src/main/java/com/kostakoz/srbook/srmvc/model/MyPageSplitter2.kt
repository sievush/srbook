package com.kostakoz.srbook.srmvc.model

import android.os.Build
import android.text.*
import android.util.Log
import com.kostakoz.srbook.MainApplication
import com.kostakoz.srbook.srmvc.model.dataclasses.Pages
import com.kostakoz.srbook.srmvc.model.dataclasses.Selected
import com.kostakoz.srbook.srmvc.model.htmlcompat.MySpannedToHtmlWIthStyleClass
import org.jsoup.Jsoup

private const val TAG = "splitter"

class MyPageSplitter2(
    private var pageWidth: Int,
    private var pageHeight: Int,
    private var lineSpacingMultiplier: Float,
    private var lineSpacingExtra: Float,
    private val textPaint: TextPaint
) {
    var i = 0
    var pages = DataFromFile2.pages
    private var mSpannableStringBuilder: SpannableStringBuilder = SpannableStringBuilder()
    private val dataBaseAdapter = DataFromFile2.dataBaseAdapter

    private val isPlus: Boolean = DataFromFile2.isPlus
    private var isChangeTextSize: Boolean = DataFromFile2.isChangeTextSize
    private var selectedList: ArrayList<Selected>? = null
    fun append(charSequence: CharSequence, id: Int, selectedList: ArrayList<Selected>?) {
        this.selectedList = selectedList
        mSpannableStringBuilder = SpannableStringBuilder()
        mSpannableStringBuilder.append(charSequence)
        split(id)
    }


    private fun split(id: Int) {
        val staticLayout =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                Log.d(TAG, "Build.VERSION_CODES.P")
                DynamicLayout.Builder.obtain(mSpannableStringBuilder, textPaint, pageWidth)
                    .setAlignment(Layout.Alignment.ALIGN_NORMAL)
                    .setLineSpacing(lineSpacingExtra, lineSpacingMultiplier)
                    .setIncludePad(false)
                    .build()
            } else {
                DynamicLayout(
                    mSpannableStringBuilder,
                    textPaint,
                    pageWidth,
                    Layout.Alignment.ALIGN_NORMAL,
                    lineSpacingMultiplier,
                    lineSpacingExtra,
                    true
                )
            }

        var startLine = 0
        while (startLine < staticLayout.lineCount) {
            val startLineTop = staticLayout.getLineTop(startLine)
            val endLine = staticLayout.getLineForVertical(startLineTop + pageHeight)
            val endLineBottom = staticLayout.getLineBottom(endLine)
            var lastFullyVisibleLine: Int
            lastFullyVisibleLine =
                if (endLineBottom > startLineTop + pageHeight) endLine - 1 else endLine
            val startOffset = staticLayout.getLineStart(startLine)
            val endOffset = staticLayout.getLineEnd(lastFullyVisibleLine)

            val onePageSpanned = mSpannableStringBuilder.subSequence(
                startOffset,
                endOffset
            ) as Spannable



            if (onePageSpanned.trim().isNotEmpty()) {
                if (isChangeTextSize) {
                    if (isPlus) {
                        if (DataFromFile2.startPositionId == id) {
                            if (DataFromFile2.startPosition in startOffset..endOffset) {
                                dataBaseAdapter.updateCurrentPage(pages!!.size)
                            }
                        }
                        if (selectedList != null) {
                            for (selected in selectedList!!.filter { it.id_data == id}) {
                                if (selected.startMain in startOffset..endOffset) {
                                    dataBaseAdapter.updateSelectedPages(selected.id, pages!!.size)
                                }
                            }
                        }
                    } else {
                        if (DataFromFile2.startPositionId == id) {
                            if (DataFromFile2.endPosition in startOffset..endOffset) {
                                dataBaseAdapter.updateCurrentPage(pages!!.size)
                            }
                        }
                        if (selectedList != null) {
                            for (selected in selectedList!!.filter { it.id_data == id}) {
                                if (selected.endMain in startOffset..endOffset) {
                                    dataBaseAdapter.updateSelectedPages(selected.id, pages!!.size)
                                }
                            }
                        }
                    }
                }

                val spannableToHTmlConverter = MySpannedToHtmlWIthStyleClass()
                val toHtml = spannableToHTmlConverter.mySpannableConvertToHtmlWithStyle(
                    MainApplication.applicationContext(),
                    onePageSpanned,
                    spannableToHTmlConverter.toHtmlParagraphLinesConsecutive
                )
                val jsoup = Jsoup.parse(toHtml)

                val page2 = Pages(
                    id = i,
                    id_data = id,
                    pages = jsoup.html(),
                    start = startOffset,
                    end = endOffset,
                    page = i,
                    text =
                    if (onePageSpanned.length > 60)
                        onePageSpanned.toString().substring(0, 59)
                    else
                        onePageSpanned.toString()
                )
                i++
                pages!!.add(page2)
            }
            startLine = lastFullyVisibleLine + 1
        }


    }
}
//996556344478

//Count else more page for one chapter!!!