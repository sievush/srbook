package com.kostakoz.srbook.srmvc.model

import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import androidx.core.text.HtmlCompat
import com.kostakoz.srbook.MainApplication
import com.kostakoz.srbook.srmvc.model.htmlcompat.MyHtmlCompat
import org.xml.sax.Attributes

val myApplication = MainApplication.applicationContext()

class HtmlToSpannable {
    fun getSpannableFromHtml(html: String): SpannableStringBuilder {
        val spannableStringBuilder = MyHtmlCompat.fromHtml(
            myApplication,
            html,
            MyHtmlCompat.FROM_HTML_SEPARATOR_LINE_BREAK_PARAGRAPH,
            ImageGetter(),
            null
        ) as SpannableStringBuilder
        return trimTrailingWhitespace(spannableStringBuilder) as SpannableStringBuilder
    }

    fun getAfterSpannableFromHtml(html: String): SpannableStringBuilder {
        val spannableStringBuilder = MyHtmlCompat.fromHtml(
            myApplication,
            html,
            HtmlCompat.FROM_HTML_MODE_LEGACY,
            ImageGetter(),
            null
        ) as SpannableStringBuilder
        return trimTrailingWhitespace(spannableStringBuilder) as SpannableStringBuilder
    }

//    fun getSpannableFromHtml(html: String): SpannableStringBuilder {
//        val spannableStringBuilder = HtmlCompat.fromHtml(
//            html,
//            HtmlCompat.FROM_HTML_MODE_LEGACY,
//            ImageGetter(),
//            null
//        ) as SpannableStringBuilder
//        return trimTrailingWhitespace(spannableStringBuilder) as SpannableStringBuilder
//    }


    private fun trimTrailingWhitespace(source: CharSequence?): CharSequence {
        if (source == null) return ""
        var i = source.length

        // loop back to the first non-whitespace character
        while (--i >= 0 && Character.isWhitespace(source[i])) {
        }
        return source.subSequence(0, i + 1)
    }

//    inner class ImageGetter : Html.ImageGetter {
//        override fun getDrawable(source: String): Drawable {
//            val inputStream = myApplication.resources.assets.open(source)
//            val d = Drawable.createFromStream(inputStream, null)
//            d.setBounds(0, 0, d.intrinsicWidth, d.intrinsicHeight)
//            return d
//        }
//    }

    inner class ImageGetter : MyHtmlCompat.ImageGetter {
        override fun getDrawable(source: String?, attributes: Attributes?): Drawable {
            val inputStream = myApplication.resources.assets.open(source!!)
            val d = Drawable.createFromStream(inputStream, source)
            d.setBounds(0, 0, d.intrinsicWidth, d.intrinsicHeight)
            return d
        }
    }
}



