package com.kostakoz.srbook.srmvc.model.savedata

import android.content.ContentValues
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import android.util.Log
import com.kostakoz.srbook.MainApplication
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import com.kostakoz.srbook.srmvc.model.HtmlToSpannable
import com.kostakoz.srbook.srmvc.model.MyPageSplitter2
import com.kostakoz.srbook.srmvc.model.dataclasses.*
import java.io.IOException

private const val TAG = "databaseadapter"
private const val CURRENT_PAGE_TABLE = "current_page"
private const val FAVORITE_PAGES_TABLE = "favorite_pages"
private const val SELECTED_PAGES_TABLE = "selected_pages"
private const val CHAPTERS_TABLE = "chapters"
private const val DATA_TABLE = "data"
private const val PAGES_TABLE = "pages"
private const val COUNT_PAGES_TABLE = "pages_count"
private const val COUNT_PAGES_AFTER_TABLE = "pages_after_count"
private const val SAVE_PAGES_TABLE = "save_pages"
private const val TEXT_SIZE_TABLE = "textsize"
private const val TYPE_FACE_TABLE = "typeface"
private const val THEME_APP_TABLE = "themeapp"
private const val ORIENTATION_SCREEN_TABLE = "orientationscreen"
private const val ANIMATION_SWIPE_PAGE_TABLE = "animationswipepage"
private const val ELSE_PAGES_TABLE = "elsePages"

class DataBaseAdapter() {
    private var mDb: SQLiteDatabase? = null
    private val mDbHelper: DataBaseHelper = DataBaseHelper(MainApplication.applicationContext())

    //    private val db = mDbHelper.writableDatabase
    @Throws(SQLException::class)
    fun createDatabase(): DataBaseAdapter {
        try {
            mDbHelper.createDataBase()
        } catch (mIOException: IOException) {
            Log.e(
                TAG,
                "$mIOException  UnableToCreateDatabase"
            )
            throw Error("UnableToCreateDatabase")
        }
        return this
    }

    @Throws(SQLException::class)
    fun open(): DataBaseAdapter {
        mDb = try {
            mDbHelper.openDataBase()
            mDbHelper.close()
            mDbHelper.readableDatabase
        } catch (mSQLException: SQLException) {
            //Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException
        }
        return this
    }

    fun close() {
//        mDbHelper.close()
//        //db.close()
    }
// SpannableString string = new SpannableString("\tParagraph text beginning with tab.");
// string.setSpan(new TabStopSpan.Standard(100), 0, string.length(),
// Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    fun getNotes(): String? {
        val query = "SELECT content FROM notes"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        var notes: String? = null
        if (cursor.moveToFirst()) {
            do {
                notes = cursor.getString(0)
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
        }
        return notes
    }

    fun getHtmlData(pageSplitter: MyPageSplitter2) {
        val query = "SELECT id, htmldata FROM $DATA_TABLE"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        if (cursor.moveToFirst()) {
            do {
                val htmlId = cursor.getInt(0)
                val htmlText = cursor.getString(1)
                val spannable = HtmlToSpannable().getSpannableFromHtml(htmlText)

//                if (DataFromFile2.currentPageText.isNotEmpty() && DataFromFile2.startPositionId == htmlId) {
//                    val start = spannable.indexOf(DataFromFile2.currentPageText)
//                    if (start != -1) {
//                        val first = spannable.subSequence(0, start) as Spannable
//                        val second = spannable.subSequence(start, spannable.length) as Spannable
//
//                        pageSplitter.append(
//                            first,
//                            htmlId
//                        )
//
//                        DataFromFile2.dataBaseAdapter.updateCurrentPage(pageSplitter.pages!!.size)
//
//                        pageSplitter.append(
//                            second,
//                            htmlId
//                        )
//                    }else {
//                        pageSplitter.append(
//                            spannable,
//                            htmlId
//                        )
//                    }
//
//                } else {
//                    pageSplitter.append(
//                        spannable,
//                        htmlId
//                    )
//                }

                pageSplitter.append(
                    spannable,
                    htmlId,
                    DataFromFile2.dataBaseAdapter.getSelectedPages()
                )

                DataFromFile2.elsePages.add(pageSplitter.pages!!.size)
//                break
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
        }
    }

    fun getOneHtmlData(id: Int): HtmlData? {
        val query =
            "SELECT id, htmldata FROM $DATA_TABLE WHERE id = '$id'"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        var htmlData: HtmlData
        return if (cursor.moveToFirst()) {
            do {
                htmlData = HtmlData(
                    id = cursor.getInt(0),
                    spannable = HtmlToSpannable().getSpannableFromHtml(cursor.getString(1))
                )
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            htmlData
        } else {
            cursor.close()
            //db.close()
            null
        }

    }

    fun updateHtmlData(
        id: Int,
        htmlData: String
    ) {
        val values = ContentValues()
        values.put("htmldata", htmlData)
        val db = mDbHelper.writableDatabase
        db.update(DATA_TABLE, values, "id='$id'", null)
        //db.close()
    }

    fun getElsePages(): ArrayList<Int>? {
        val query = "SELECT count FROM $ELSE_PAGES_TABLE"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        val list = ArrayList<Int>()
        return if (cursor.moveToFirst()) {
            do {
                list.add(cursor.getInt(0))
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            list
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun setElsePages(
        count: Int
    ) {
        val values = ContentValues()
        values.put("count", count)
        val db = mDbHelper.writableDatabase
        db.insert(ELSE_PAGES_TABLE, null, values)
        //db.close()
    }

    fun deleteElsePages() {
        val db = mDbHelper.writableDatabase
        db.execSQL("DELETE FROM $ELSE_PAGES_TABLE");
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '$ELSE_PAGES_TABLE'")
    }

    //"SELECT content FROM data WHERE id = '$id'"
//"SELECT item_id, txt, cat_id FROM items WHERE saved = 1 AND txt like '%$find%'"
    fun getChapters(): ArrayList<Chapters>? {
        val query = "SELECT id, depth, title FROM $CHAPTERS_TABLE"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        val list = ArrayList<Chapters>()
        return if (cursor.moveToFirst()) {
            do {
                val chapters = Chapters(
                    id = cursor.getInt(0),
                    depth = cursor.getInt(1),
                    title = cursor.getString(2)
                )
                list.add(chapters)
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            list
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun getOneChapters(id: Int): Chapters? {
        val query = "SELECT id, depth, title FROM $CHAPTERS_TABLE WHERE id = '$id'"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        var chapters: Chapters
        return if (cursor.moveToFirst()) {
            do {
                chapters = Chapters(
                    id = cursor.getInt(0),
                    depth = cursor.getInt(1),
                    title = cursor.getString(2)
                )

            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            chapters
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun getCountPages(): Int {
        val query = "SELECT count FROM $COUNT_PAGES_TABLE"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        var count = 0
        return if (cursor.moveToFirst()) {
            do {
                count = cursor.getInt(0)
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            count
        } else {
            cursor.close()
            //db.close()
            0
        }
    }

    fun setCountPages(
        count: Int
    ) {
        val values = ContentValues()
        values.put("count", count)
        val db = mDbHelper.writableDatabase
        db.update(COUNT_PAGES_TABLE, values, "id=0", null)
        //db.close()
    }

    fun getAfterCountPages(): Int {
        val query = "SELECT count FROM $COUNT_PAGES_AFTER_TABLE"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        var count = 0
        return if (cursor.moveToFirst()) {
            do {
                count = cursor.getInt(0)
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            count
        } else {
            cursor.close()
            //db.close()
            0
        }
    }

    fun setAfterCountPages(
        count: Int
    ) {
        val values = ContentValues()
        values.put("count", count)
        val db = mDbHelper.writableDatabase
        db.update(COUNT_PAGES_AFTER_TABLE, values, "id=0", null)
        //db.close()
    }

    fun getPages(): ArrayList<Pages>? {
        val query = "SELECT id, id_data, pages, start, `end`, page, text FROM $PAGES_TABLE"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        val list = ArrayList<Pages>()
        return if (cursor.moveToFirst()) {
            do {
                val page = Pages(
                    id = cursor.getInt(0),
                    id_data = cursor.getInt(1),
                    pages = cursor.getString(2),
                    start = cursor.getInt(3),
                    end = cursor.getInt(4),
                    page = cursor.getInt(5),
                    text = cursor.getString(6)
                )
                list.add(page)
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            list
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun getOnePage(page: Int): Pages? {
        val query =
            "SELECT id, id_data, pages, start, `end`, page, text FROM $PAGES_TABLE WHERE page = '$page'"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        var page: Pages
        return if (cursor.moveToFirst()) {
            do {
                page = Pages(
                    id = cursor.getInt(0),
                    id_data = cursor.getInt(1),
                    pages = cursor.getString(2),
                    start = cursor.getInt(3),
                    end = cursor.getInt(4),
                    page = cursor.getInt(5),
                    text = cursor.getString(6)
                )

            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            page
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun getPageWithId(id: Int): Pages? {
        val query =
            "SELECT id, id_data, pages, start, `end`, page, text FROM $PAGES_TABLE WHERE id = '$id'"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        var page: Pages
        return if (cursor.moveToFirst()) {
            do {
                page = Pages(
                    id = cursor.getInt(0),
                    id_data = cursor.getInt(1),
                    pages = cursor.getString(2),
                    start = cursor.getInt(3),
                    end = cursor.getInt(4),
                    page = cursor.getInt(5),
                    text = cursor.getString(6)
                )

            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            page
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun setPages(
        id: Int,
        id_data: Int,
        pages: String,
        start: Int,
        end: Int,
        page: Int,
        text: String
    ) {
        val values = ContentValues()
        values.put("id", id)
        values.put("id_data", id_data)
        values.put("pages", pages)
        values.put("start", start)
        values.put("end", end)
        values.put("page", page)
        values.put("text", text)
        val db = mDbHelper.writableDatabase
        db.insert(PAGES_TABLE, null, values)
        //db.close()
    }

    fun deletePages() {
        val db = mDbHelper.writableDatabase
        db.execSQL("DELETE FROM $PAGES_TABLE");
        db.execSQL("DELETE FROM SQLITE_SEQUENCE WHERE NAME = '$PAGES_TABLE'")
    }

    fun updatePages(
        id: Int,
        pages: String
    ) {
        val values = ContentValues()
        values.put("pages", pages)
        val db = mDbHelper.writableDatabase
        db.update(PAGES_TABLE, values, "id=$id", null)
        //db.close()
    }


    fun getSelectedPages(): ArrayList<Selected>? {
        val query =
            "SELECT id, id_data, favorite, start, `end`, startMain, endMain, page, color, text, description FROM $SELECTED_PAGES_TABLE"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        val list = ArrayList<Selected>()
        return if (cursor.moveToFirst()) {
            do {
                val selected = Selected(
                    id = cursor.getInt(0),
                    id_data = cursor.getInt(1),
                    favorite = cursor.getInt(2),
                    start = cursor.getInt(3),
                    end = cursor.getInt(4),
                    startMain = cursor.getInt(5),
                    endMain = cursor.getInt(6),
                    page = cursor.getInt(7),
                    color = cursor.getInt(8),
                    text = cursor.getString(9),
                    description = cursor.getString(10)
                )
                list.add(selected)
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            list
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun getOneSelectedPages(page: Int): Selected? {
        val query =
            "SELECT id, id_data, favorite, start, `end`, startMain, endMain, page, color, text, description FROM $SELECTED_PAGES_TABLE WHERE page = '$page' and favorite = '1'"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        var selected: Selected
        return if (cursor.moveToFirst()) {
            do {
                selected = Selected(
                    id = cursor.getInt(0),
                    id_data = cursor.getInt(1),
                    favorite = cursor.getInt(2),
                    start = cursor.getInt(3),
                    end = cursor.getInt(4),
                    startMain = cursor.getInt(5),
                    endMain = cursor.getInt(6),
                    page = cursor.getInt(7),
                    color = cursor.getInt(8),
                    text = cursor.getString(9),
                    description = cursor.getString(10)
                )

            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            selected
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun setSelectedPages(
        favorite: Int,
        id_data: Int,
        start: Int,
        end: Int,
        startMain: Int,
        endMain: Int,
        page: Int,
        color: Int = Color.GRAY,
        text: String,
        description: String
    ) {
        val values = ContentValues()
        values.put("favorite", favorite)
        values.put("id_data", id_data)
        values.put("start", start)
        values.put("end", end)
        values.put("startMain", startMain)
        values.put("endMain", endMain)
        values.put("page", page)
        values.put("color", color)
        values.put("text", text)
        values.put("description", description)
        val db = mDbHelper.writableDatabase
        db.insert(SELECTED_PAGES_TABLE, null, values)
        //db.close()
    }

    fun deleteSelectedPages(
        id: Int
    ) {
        val db = mDbHelper.writableDatabase
        db.execSQL("DELETE FROM $SELECTED_PAGES_TABLE WHERE id = '$id'")
        //db.close()
    }

    fun updateSelectedPages(
        id: Int,
        page: Int
    ) {
        val values = ContentValues()
        values.put("page", page)
        val db = mDbHelper.writableDatabase
        db.update(SELECTED_PAGES_TABLE, values, "id = $id", null)
        //db.close()
    }

    fun updateSelectedDescription(
        id: Int,
        description: String
    ) {
        val values = ContentValues()
        values.put("description", description)
        val db = mDbHelper.writableDatabase
        db.update(SELECTED_PAGES_TABLE, values, "id = $id", null)
        //db.close()
    }

    fun getCurrentPage(): Int {
        val query =
            "SELECT page FROM $CURRENT_PAGE_TABLE"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        return if (cursor.moveToFirst()) {
            val page = cursor.getInt(0)
            cursor.close()
            //db.close()
            page
        } else {
            cursor.close()
            //db.close()
            0
        }
    }

    fun updateCurrentPage(current: Int) {
        val values = ContentValues()
        values.put("page", current)
        val db = mDbHelper.writableDatabase
        db.update(CURRENT_PAGE_TABLE, values, "id=1", null)
        //db.close()
    }

    fun getTitle(): String {
        val query =
            "SELECT title FROM title"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        return if (cursor.moveToFirst()) {
            val title = cursor.getString(0)
            cursor.close()
            title
        } else {
            cursor.close()
            ""
        }
    }

    fun getDate(): String {
        val query =
            "SELECT date FROM date"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        return if (cursor.moveToFirst()) {
            val date = cursor.getString(0)
            cursor.close()
            date
        } else {
            cursor.close()
            ""
        }
    }

    fun getLang(): String {
        val query =
            "SELECT lang FROM language"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        return if (cursor.moveToFirst()) {
            val lang = cursor.getString(0)
            cursor.close()
            lang
        } else {
            cursor.close()
            ""
        }
    }

    fun getDescriptions(): String {
        val query =
            "SELECT `desc` FROM descriptions"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        return if (cursor.moveToFirst()) {
            val lang = cursor.getString(0)
            cursor.close()
            lang
        } else {
            cursor.close()
            ""
        }
    }

    fun getPublishers(): String {
        val query =
            "SELECT publishers FROM publishers"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        return if (cursor.moveToFirst()) {
            val publishers = cursor.getString(0)
            cursor.close()
            publishers
        } else {
            cursor.close()
            ""
        }
    }

    fun getAuthors(): ArrayList<Authors>? {
        val query =
            "SELECT firstname, lastname, who FROM authors"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        val list: ArrayList<Authors> = ArrayList()
        return if (cursor.moveToFirst()) {
            do {
                val firstname = cursor.getString(0)
                val lastname = cursor.getString(1)
                val who = cursor.getString(2)
                list.add(Authors(firstname, lastname, who))
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            list
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

    fun getSubjects(): ArrayList<String>? {
        val query =
            "SELECT subs FROM subjects"
        val db = mDbHelper.writableDatabase
        val cursor = db.rawQuery(query, null)
        val list: ArrayList<String> = ArrayList()
        return if (cursor.moveToFirst()) {
            do {
                val subs = cursor.getString(0)
                list.add(subs)
            } while (cursor.moveToNext())
            cursor.close()
            //db.close()
            list
        } else {
            cursor.close()
            //db.close()
            null
        }
    }

//    fun updateCurrentTextSize(current: Int) {
//        val values = ContentValues()
//        values.put("current", current)
//        val db = mDbHelper.writableDatabase
//        db.update(TEXT_SIZE_TABLE, values, "id=0", null)
//    }
//
//    fun getCurrentTypeFace(): Int {
//        val query =
//            "SELECT current FROM $TYPE_FACE_TABLE"
//        val db = mDbHelper.writableDatabase
//        val cursor = db.rawQuery(query, null)
//        return if (cursor.moveToFirst()) {
//            val page = cursor.getInt(0)
//            cursor.close()
//            page
//        } else {
//            cursor.close()
//            0
//        }
//    }
//
//    fun updateCurrentTypeFace(current: Int) {
//        val values = ContentValues()
//        values.put("current", current)
//        val db = mDbHelper.writableDatabase
//        db.update(TYPE_FACE_TABLE, values, "id=0", null)
//    }
//
//    fun getCurrentThemeApp(): Int {
//        val query =
//            "SELECT current FROM $THEME_APP_TABLE"
//        val db = mDbHelper.writableDatabase
//        val cursor = db.rawQuery(query, null)
//        return if (cursor.moveToFirst()) {
//            val page = cursor.getInt(0)
//            cursor.close()
//            page
//        } else {
//            cursor.close()
//            0
//        }
//    }
//
//    fun updateCurrentThemeApp(current: Int) {
//        val values = ContentValues()
//        values.put("current", current)
//        val db = mDbHelper.writableDatabase
//        db.update(THEME_APP_TABLE, values, "id=0", null)
//    }
//
//    fun getCurrentOrientationScreen(): Int {
//        val query =
//            "SELECT current FROM $ORIENTATION_SCREEN_TABLE"
//        val db = mDbHelper.writableDatabase
//        val cursor = db.rawQuery(query, null)
//        return if (cursor.moveToFirst()) {
//            val page = cursor.getInt(0)
//            cursor.close()
//            page
//        } else {
//            cursor.close()
//            0
//        }
//    }
//
//    fun updateCurrentOrientationScreen(current: Int) {
//        val values = ContentValues()
//        values.put("current", current)
//        val db = mDbHelper.writableDatabase
//        db.update(ORIENTATION_SCREEN_TABLE, values, "id=0", null)
//    }
//
//    fun getCurrentAnimationSwipePage(): Int {
//        val query =
//            "SELECT current FROM $ANIMATION_SWIPE_PAGE_TABLE"
//        val db = mDbHelper.writableDatabase
//        val cursor = db.rawQuery(query, null)
//        return if (cursor.moveToFirst()) {
//            val page = cursor.getInt(0)
//            cursor.close()
//            page
//        } else {
//            cursor.close()
//            0
//        }
//    }
//
//    fun updateCurrentAnimationSwipePage(current: Int) {
//        val values = ContentValues()
//        values.put("current", current)
//        val db = mDbHelper.writableDatabase
//        db.update(ANIMATION_SWIPE_PAGE_TABLE, values, "id=0", null)
//    }

    companion object {
        private const val TAG = "DataAdapter"
    }

}