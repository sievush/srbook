package com.kostakoz.srbook.srmvc.model
//
//
////import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread
//import android.annotation.SuppressLint
//import android.os.Bundle
//import android.util.Log
//import android.util.TypedValue
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.fragment.app.Fragment
//import com.kostakoz.srbook.srmvc.model.TextSize
//import org.jsoup.Jsoup
//
//private const val PAGE_TEXT = "PAGE_TEXT"
//private const val MY_FRAGMENT = "myFragment"
//
//class MyFragment : Fragment() {
//    //    private lateinit var textView: AppCompatTextView

//    private var isSelection = false
//    fun newInstance(pageText: String): MyFragment {
//        val myFragment = MyFragment()
//        val arg = Bundle()
//        arg.putString(PAGE_TEXT, pageText)
//        myFragment.arguments = arg
//        return myFragment
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
////        setHasOptionsMenu(true)
//        return inflater.inflate(R.layout.for_fragment, container, false)
//    }
//
////    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
////        inflater.inflate(R.menu.main2, menu)
////        super.onCreateOptionsMenu(menu, inflater)
////    }
//

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

////        textView = view.findViewById(R.id.textView)
////        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
////            textView.justificationMode = JUSTIFICATION_MODE_INTER_WORD
////            //        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.resources.getDimension(R.dimen.text_size))
////
////        }
////        textView.setTextSize(
////            TypedValue.COMPLEX_UNIT_PX,
////            TextSize.textSizeList[TextSize.currentTextSize]
////        )
////        val text =
////            arguments!!.getString(PAGE_TEXT)?.let { HtmlToSpannable().getSpannableFromHtml(it) }
//////        Log.d("checkFragment", text.toString())
////        textView.text = text
////        textView.movementMethod = LinkMovementMethod.getInstance()
////        textView.customSelectionActionModeCallback = MyCallBack()
////        val fullscreenActivity: FullscreenActivity = activity as FullscreenActivity
////        textView.setOnClickListener {
////            if (!isSelection) {
////                Log.d(PAGE_TEXT, "onlick")
////                fullscreenActivity.toggle()
////            }
////            else isSelection = false
////        }
//
//    }
//
//
//    inner class MyCallBack : ActionMode.Callback {
//
//        private fun commentText(start: Int, end: Int, color: Int, spannable: Spannable) {
//            val dialogBuilder: AlertDialog.Builder = AlertDialog.Builder(activity!!)
//            val inflater = activity!!.layoutInflater
//            val dialogView: View = inflater.inflate(R.layout.comment_text, null)
//            dialogBuilder.setView(dialogView)
//            val editText = dialogView.findViewById<EditText>(R.id.commentTextComment)
//            val save = dialogView.findViewById<Button>(R.id.saveComment)
//            val cancel = dialogView.findViewById<Button>(R.id.cancelComment)
//            val alertDialog = dialogBuilder.create()
//            alertDialog.show()
//
//            save.setOnClickListener {
//                saveToDataBase(spannable, start, end, color, editText.text.toString())
//                alertDialog.dismiss()
//            }
//            cancel.setOnClickListener {
//
//                alertDialog.dismiss()
//            }
//
//        }
//
//        private fun saveToDataBase(
//            spannable: Spannable,
//            start: Int,
//            end: Int,
//            color: Int,
//            description: String = ""
//        ) {
//            spannable.setSpan(
//                BackgroundColorSpan(color), start
//                , end,
//                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//            )
//            val dataBaseAdapter = DataBaseAdapter()
//            dataBaseAdapter.createDatabase()
//            dataBaseAdapter.open()
//            val currentPage = dataBaseAdapter.getCurrentPage()
//            val savePages = DataFromFile2.pages.first { it.page == currentPage }
//            dataBaseAdapter.updatePages(savePages.id, Html.toHtml(spannable))
//            dataBaseAdapter.setSelectedPages(
//                id_data = savePages.id,
//                start = savePages.start + start,
//                end = savePages.start + end,
//                page = currentPage,
//                color = color,
//                text = spannable.subSequence(start, end).toString(),
//                description = description
//            )
//            dataBaseAdapter.close()
//        }
//
//        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
//            val start = textView.selectionStart
//            val end = textView.selectionEnd
//            val wordSpan: Spannable = textView.text as Spannable
//
//            if (start == 0 && end == 0)
//                return false
//            if (start < 0 && end < 0)
//                return false
//            if (start > end)
//                return false
//            if (end < start)
//                return false
//
//            val color = Color.rgb(179, 229, 252)
//
//
//            return when (item?.itemId) {
//                R.id.selected -> {
//                    saveToDataBase(wordSpan, start, end, color)
//                    mode?.finish()
//                    true
//                }
//                R.id.commented -> {
//                    val selectedColor = Color.rgb(220, 231, 117)
//                    commentText(start, end, selectedColor, wordSpan)
//                    mode?.finish()
//                    true
//                }
//                android.R.id.copy -> {
//                    val myClipboard =
//                        context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
//                    val myClip: ClipData = ClipData.newPlainText(
//                        "note_copy",
//                        textView.text.subSequence(start, end).toString()
//                    )
//                    myClipboard.setPrimaryClip(myClip)
//                    mode?.finish()
//                    true
//                }
//                android.R.id.shareText -> {
//                    val sendIntent: Intent = Intent().apply {
//                        action = Intent.ACTION_SEND
//                        putExtra(
//                            Intent.EXTRA_TEXT,
//                            textView.text.subSequence(start, end).toString()
//                        )
//                        type = "text/plain"
//                    }
//
//                    val shareIntent = Intent.createChooser(sendIntent, null)
//                    startActivity(shareIntent)
//
//                    mode?.finish()
//                    true
//                }
//
//                else -> false
//            }
//
//
//        }
//
//        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
//            Log.d(PAGE_TEXT, "onCreateActionMode")
//            isSelection = true
//            // menu?.clear()
//            menu?.removeItem(android.R.id.selectAll)
//            menu?.removeItem(android.R.id.paste)
//            val inflater: MenuInflater = mode!!.menuInflater
//            inflater.inflate(R.menu.context_menu, menu)
//            return true
//        }
//
//        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
//
//            return false
//        }
//
//        override fun onDestroyActionMode(mode: ActionMode?) {
//            Log.d(PAGE_TEXT, "onDestroyActionMode")
////            isSelection = false
//        }
//
//
//    }
//
//
//}
//
//
////            menu?.removeItem(android.R.id.copy)
////            menu?.removeItem(android.R.id.paste)
////            menu?.removeItem(android.R.id.selectAll)
////            menu?.removeItem(android.R.id.paste)
////            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
////                menu?.removeItem(android.R.id.shareText)
////            }