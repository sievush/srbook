package com.kostakoz.srbook.srmvc.model

import android.content.res.Resources
import com.kostakoz.srbook.R

class TypeFace {
    companion object {

        val fontFaceStyle = """
            @font-face {
              font-family: 'Bitter-Regular';
              src: url('typefaces/Bitter-Regular.ttf');
            }
            @font-face {
              font-family: 'Arvo';
              src: url('typefaces/Arvo.ttf');
            }
            @font-face {
              font-family: 'Open Sans';
              src: url('typefaces/Open Sans.ttf');
            }
            @font-face {
              font-family: 'PlayfairDisplay-Medium';
              src: url('typefaces/PlayfairDisplay-Medium.ttf');
            }
            @font-face {
              font-family: 'PT Sans';
              src: url('typefaces/PT Sans.ttf');
            }
            @font-face {
              font-family: 'Roboto';
              src: url('typefaces/Roboto.ttf');
            }
            @font-face {
              font-family: 'Source Sans Pro';
              src: url('typefaces/Source Sans Pro.ttf');
            }
            @font-face {
              font-family: 'Ubuntu';
              src: url('typefaces/Ubuntu.ttf');
            }
            @font-face {
              font-family: 'ZillaSlab-Medium';
              src: url('typefaces/ZillaSlab-Medium.ttf');
            }
        """.trimIndent()

        val typefaceList = listOf(
            TypeFaceMyClass(
                0,
                "System TypeFace",
                ""
            ),
            TypeFaceMyClass(
                1,
                "Bitter-Regular",
                "typefaces/Bitter-Regular.ttf"
            ),
            TypeFaceMyClass(
                2,
                "Arvo",
                "typefaces/Arvo.ttf"
            ),
            TypeFaceMyClass(
                3,
                "Open Sans",
                "typefaces/Open Sans.ttf"
            ),
            TypeFaceMyClass(
                4,
                "PlayfairDisplay-Medium",
                "typefaces/PlayfairDisplay-Medium.ttf"
            ),
            TypeFaceMyClass(
                5,
                "PT Sans",
                "typefaces/PT Sans.ttf"
            ),
            TypeFaceMyClass(
                6,
                "Roboto",
                "typefaces/Roboto.ttf"
            ),
            TypeFaceMyClass(
                7,
                "Source Sans Pro",
                "typefaces/Source Sans Pro.ttf"
            ),
            TypeFaceMyClass(
                8,
                "Ubuntu",
                "typefaces/Ubuntu.ttf"
            ),
            TypeFaceMyClass(
                9,
                "ZillaSlab-Medium",
                "typefaces/ZillaSlab-Medium.ttf"
            )
        )

        data class TypeFaceMyClass(
            val id: Int,
            val name: String,
            val path: String
        )
    }
}