package com.kostakoz.srbook.srmvc.model.savedata

import android.content.Context
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream

class DataBaseHelper(context: Context) :
    SQLiteOpenHelper(context, DB_NAME, null, 2) {
    private var mDataBase: SQLiteDatabase? = null
    private val mContext: Context

    @Throws(IOException::class)
    fun createDataBase() {
        var mDataBaseExist = false
        //If database not exists copy it from the assets
        mDataBaseExist = checkDataBase()
        if (!mDataBaseExist) {
            this.readableDatabase
            close()
            try {
                //Copy the database from assests
                copyDataBase()
            } catch (mIOException: IOException) {
                throw Error("ErrorCopyingDataBase")
            }
        }
    }

    //Check that the database exists here: /data/data/your package/databases/Da Name
    private fun checkDataBase(): Boolean {
        val dbFile =
            File(DB_PATH + DB_NAME)
        Log.v("dbFile", dbFile.toString() + "   " + dbFile.exists())
        return dbFile.exists()
    }

    //Copy the database from assets
    @Throws(IOException::class)
    private fun copyDataBase() {

        val mInput =
            mContext.assets.open(DB_NAME)
        val outFileName =
            DB_PATH + DB_NAME
        val mOutput: OutputStream = FileOutputStream(outFileName)
        val mBuffer = ByteArray(1024)
        var mLength: Int
        while (mInput.read(mBuffer).also { mLength = it } > 0) {
            mOutput.write(mBuffer, 0, mLength)
        }
        mOutput.flush()
        mOutput.close()
        mInput.close()
    }

    //Open the database, so we can query it
    @Throws(SQLException::class)
    fun openDataBase(): Boolean {
        val mPath =
            DB_PATH + DB_NAME
        Log.v("mPath", mPath)
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY)
        //mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        return mDataBase != null
    }

    @Synchronized
    override fun close() {
        if (mDataBase != null) mDataBase!!.close()
        super.close()
    }

    override fun onCreate(arg0: SQLiteDatabase) {
        // TODO Auto-generated method stub
    }

    override fun onUpgrade(
        arg0: SQLiteDatabase,
        oldVersion: Int,
        newVersion: Int
    ) {
        if(newVersion>oldVersion){
            copyDataBase()
        }
        Log.e("onUpgrade", "onUpgrade")
    }

    companion object {
        private const val TAG = "DataBaseHelper" // Tag just for the LogCat window

        //destination path (location) of our database on device
        private var DB_PATH = ""
        private const val DB_NAME = "properties.db" // Database name
    }

    init {
        DB_PATH = if (Build.VERSION.SDK_INT >= 17) {
            context.applicationInfo.dataDir + "/databases/"
        } else {
            "/data/data/" + context.packageName + "/databases/"
        }
        mContext = context
    }
}