package com.kostakoz.srbook.srmvc.model

import android.content.res.Resources
import com.kostakoz.srbook.MainApplication
import com.kostakoz.srbook.R

class TextSize {
    companion object {
        private val resources = MainApplication.applicationContext().resources
        var textSizeList = listOf(
            resources.getDimension(R.dimen.text_size_1),
            resources.getDimension(R.dimen.text_size_2),
            resources.getDimension(R.dimen.text_size_3),
            resources.getDimension(R.dimen.text_size_4),
            resources.getDimension(R.dimen.text_size_5),
            resources.getDimension(R.dimen.text_size_6),
            resources.getDimension(R.dimen.text_size_7),
            resources.getDimension(R.dimen.text_size_8),
            resources.getDimension(R.dimen.text_size_9),
            resources.getDimension(R.dimen.text_size_10),
            resources.getDimension(R.dimen.text_size_11)
        )

        var textSizeListWeb = listOf(
            17,
            20,
            23,
            25,
            27,
            29,
            31,
            33,
            35,
            37,
            40
        )
    }
}