package com.kostakoz.srbook.srmvc.model

class LineSpace {
    companion object {
        var lineSpaceList = listOf(
            1f, 1.2f, 1.4f, 1.6f, 1.8f, 2f, 2.2f, 2.4f, 2.6f, 2.8f, 3f
        )

        var lineSpaceListWeb = listOf(
             "1.1", "1.4", "1.6", "1.9", "2.1", "2.2", "2.5", "2.7", "2.9", "3.5", "4.0"
        )
    }
}