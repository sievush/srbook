package com.kostakoz.srbook.srmvc.model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.Toast;

import java.lang.reflect.Method;


public class MyWebView extends WebView {
private final static String TAG = "MyWebViewTag";
    public MyWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initView(context);
    }

    public MyWebView(Context context) {
        super(context);
        initView(context);
    }

    public MyWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);

    }


    @Override
    public ActionMode startActionMode(ActionMode.Callback callback) {
//        Toast.makeText(getContext(), "StartActionModeWEB1", Toast.LENGTH_SHORT).show();
//        loadUrl("javascript:testEcho();");
        Log.d(TAG, "startActionMode");
        return new ActionMode() {
            @Override
            public void setTitle(CharSequence title) {
                Log.d(TAG, "setTitle");
            }

            @Override
            public void setTitle(int resId) {
                Log.d(TAG, "setTitle2");
            }

            @Override
            public void setSubtitle(CharSequence subtitle) {
                Log.d(TAG, "setSubtitle");
            }

            @Override
            public void setSubtitle(int resId) {
                Log.d(TAG, "setSubtitle2");
            }

            @Override
            public void setCustomView(View view) {
                Log.d(TAG, "setCustomView");
            }

            @Override
            public void invalidate() {
                Log.d(TAG, "invalidate");
            }

            @Override
            public void finish() {
                Log.d(TAG, "finish");
            }

            @Override
            public Menu getMenu() {
                Log.d(TAG, "getMenu");
                return null;
            }

            @Override
            public CharSequence getTitle() {
                Log.d(TAG, "getTitle");
                return null;
            }

            @Override
            public CharSequence getSubtitle() {
                Log.d(TAG, "getSubtitle");
                return null;
            }

            @Override
            public View getCustomView() {
                Log.d(TAG, "getCustomView");
                return null;
            }

            @Override
            public MenuInflater getMenuInflater() {
                Log.d(TAG, "getMenuInflater");
                return null;
            }
        };
    }
//
    @Override
    public ActionMode startActionMode(ActionMode.Callback callback, int type) {
//        Toast.makeText(getContext(), "StartActionModeWEB2", Toast.LENGTH_LONG).show();
        return new ActionMode() {
            @Override
            public void setTitle(CharSequence title) {
            }

            @Override
            public void setTitle(int resId) {
            }

            @Override
            public void setSubtitle(CharSequence subtitle) {
            }

            @Override
            public void setSubtitle(int resId) {
            }

            @Override
            public void setCustomView(View view) {
            }

            @Override
            public void invalidate() {
            }

            @Override
            public void finish() {
            }

            @Override
            public Menu getMenu() {
                return null;
            }

            @Override
            public CharSequence getTitle() {
                return null;
            }

            @Override
            public CharSequence getSubtitle() {
                return null;
            }

            @Override
            public View getCustomView() {
                return null;
            }

            @Override
            public MenuInflater getMenuInflater() {
                return null;
            }
        };
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initView(Context context){
        // i am not sure with these inflater lines
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // you should not use a new instance of MyWebView here
        // MyWebView view = (MyWebView) inflater.inflate(R.layout.custom_webview, this);
//        this.getSettings().setJavaScriptEnabled(true);
////        this.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
//        this.getSettings().setUseWideViewPort(true);
//        this.getSettings().setLoadWithOverviewMode(true);
////        this.getSettings().setDomStorageEnabled(true);
//        this.getSettings().setDefaultTextEncodingName("utf-8");

//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            this.getSettings().setMixedContentMode(0);
//            this.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        } else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            this.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        } else {
//            this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        }

    }

    @Override
    public void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.scrollTo(0, 0);
    }

    @Override
    public boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY,
                                int scrollRangeX, int scrollRangeY, int maxOverScrollX,
                                int maxOverScrollY, boolean isTouchEvent) {
        return false;
    }

//    public void getRealHeight(){
//        Display display = context.getWindowManager().getDefaultDisplay();
//        int realWidth;
//        int realHeight;
//
//        if (Build.VERSION.SDK_INT >= 17){
//            //new pleasant way to get real metrics
//            DisplayMetrics realMetrics = new DisplayMetrics();
//            display.getRealMetrics(realMetrics);
//            realWidth = realMetrics.widthPixels;
//            realHeight = realMetrics.heightPixels;
//
//        } else if (Build.VERSION.SDK_INT >= 14) {
//            //reflection for this weird in-between time
//            try {
//                Method mGetRawH = Display.class.getMethod("getRawHeight");
//                Method mGetRawW = Display.class.getMethod("getRawWidth");
//                realWidth = (Integer) mGetRawW.invoke(display);
//                realHeight = (Integer) mGetRawH.invoke(display);
//            } catch (Exception e) {
//                //this may not be 100% accurate, but it's all we've got
//                realWidth = display.getWidth();
//                realHeight = display.getHeight();
//                Log.e("Display Info", "Couldn't use reflection to get the real display metrics.");
//            }
//
//        } else {
//            //This should be close, as lower API devices should not have window navigation bars
//            realWidth = display.getWidth();
//            realHeight = display.getHeight();
//        }
//    }

}
