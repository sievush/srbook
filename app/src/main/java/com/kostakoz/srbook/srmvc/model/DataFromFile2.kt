package com.kostakoz.srbook.srmvc.model

import android.text.TextPaint
import androidx.fragment.app.Fragment
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.dataclasses.*
import com.kostakoz.srbook.srmvc.model.savedata.DataBaseAdapter
import java.lang.StringBuilder

private const val MyLog = "dataFromFile"

class DataFromFile2 {
    companion object {

        var startPosition: Int = 0
        var endPosition: Int = 0
        var startPositionId: Int = 0
        var currentPageText = ""

        var pages: ArrayList<Pages>? = ArrayList()
        var elsePages = ArrayList<Int>()

        var isChangeTextSize: Boolean = false
        var isPlus: Boolean = false

        val fragmentMap = HashMap<String, MyFragment>()

        lateinit var dataBaseAdapter: DataBaseAdapter

        fun doPagesForViewPager2(
            textPaint: TextPaint,
            width: Int,
            height: Int
        ) {

            pages?.clear()
            elsePages.clear()
            pages = ArrayList()
            dataBaseAdapter.deletePages()
            val pageSplitter =
                MyPageSplitter2(
                    width,
                    height,
                    LineSpace.lineSpaceList[MyPref.getLineSpace()],
                    0f,
                    textPaint
                )

            dataBaseAdapter.getHtmlData(pageSplitter)
        }
    }
}