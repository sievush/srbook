package com.kostakoz.srbook.srmvc.model.dataclasses

data class Authors(
    val firstname: String,
    val lastname: String,
    val who: String
)