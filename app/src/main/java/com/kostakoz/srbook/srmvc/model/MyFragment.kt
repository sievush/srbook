package com.kostakoz.srbook.srmvc.model

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.text.Editable
import android.text.Spannable
import android.text.TextWatcher
import android.text.style.BackgroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.webkit.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.kostakoz.srbook.FullscreenActivity
import com.kostakoz.srbook.MainApplication
import com.kostakoz.srbook.R
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.htmlcompat.MySpannedToHtmlWIthStyleClass
import org.jsoup.Jsoup

private const val PAGE_POSITION = "PAGE_POSITION"
private const val MY_FRAGMENT = "myFragment"
private lateinit var viewFragment: View

class MyFragment : Fragment() {

    private lateinit var webView: MyWebView
    private lateinit var fullscreenActivity: FullscreenActivity
    private lateinit var progressBar: ProgressBar

    fun newInstance(position: Int): MyFragment {
        val myFragment = MyFragment()
        val arg = Bundle()
        arg.putInt(PAGE_POSITION, position)
        myFragment.arguments = arg
        return myFragment
    }

    override fun onDestroyView() {
        super.onDestroyView()
        webView.clearHistory()
        webView.clearCache(true)
        webView.loadUrl("about:blank")
        WebStorage.getInstance().deleteAllData()
        webView.destroy()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        selectedColor = requireActivity().resources.getColor(R.color.saveColor)//Color.rgb(179, 229, 252)
        selectedColorD = requireActivity().resources.getColor(R.color.commentColor)//Color.rgb(220, 231, 117)
        return inflater.inflate(R.layout.for_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("taglog", tag!!)
        DataFromFile2.fragmentMap[tag!!] = this
        viewFragment = view
        webView = view.findViewById(
            R.id.webview
        )
        webView.setBackgroundColor(Color.TRANSPARENT)

        val margins = (webView.layoutParams as ViewGroup.MarginLayoutParams).apply {
            leftMargin = LeftRightMargins.leftRightMarginsList[MyPref.getRightLeftMargins()]
            rightMargin = LeftRightMargins.leftRightMarginsList[MyPref.getRightLeftMargins()]
        }
        webView.layoutParams = margins

        fullscreenActivity = activity as FullscreenActivity
        progressBar = view.findViewById(R.id.progressBar)

        val dataBaseAdapter = DataFromFile2.dataBaseAdapter
        val position = requireArguments().getInt(PAGE_POSITION)
        val currentHtmlPage =
            dataBaseAdapter.getOnePage(position) ?: DataFromFile2.pages?.get(position) ?: return

        loadWeb(currentHtmlPage.pages)

    }

    inner class MyWebClient : WebViewClient() {
//        override fun shouldOverrideUrlLoading(
//            view: WebView?,
//            request: WebResourceRequest?
//        ): Boolean {
//            val i = Intent(Intent.ACTION_VIEW, request?.url)
//            startActivity(i)
//            return true
//        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
//            Toast.makeText(requireContext(), url, Toast.LENGTH_SHORT).show()
            val document = Jsoup.parse(DataFromFile2.dataBaseAdapter.getNotes())
            return when {
                url!!.indexOf("#") != -1 -> {
                    val id = url.split("#")[1]
                    val note = document.getElementById(id)
                    val build = AlertDialog.Builder(requireActivity())
                    build.setMessage(note.getElementsByTag("p")[0].text())
                    val dialog = build.create()
                    dialog.show()
                    true
                }
                else -> true
            }
        }

        override fun onPageFinished(view: WebView?, url: String?) {
//            view?.loadUrl("javascript:Android.resize(document.body.scrollHeight)");
            val appcontext = MainApplication.applicationContext()
            val height = (view!!.height / appcontext.resources.displayMetrics.density).toInt()
            val width = (view.width / appcontext.resources.displayMetrics.density).toInt()

            val javascript = """
                javascript: autoTextSizeMyWebview($height, $width);
                function autoTextSizeMyWebview(webViewHeight, webViewWidth){
                    var currentFontSize = parseFloat(jQuery("p").css("font-size")); 
                    var currentHeight = parseInt(jQuery("body").height());  
                    var i = 0;
                     
                    while(currentHeight > webViewHeight){
                        i = i + 1;
                        if(i >= 100){
                            break;
                        } 
                        currentFontSize = (currentFontSize - 0.1).toFixed(2);
            
                        jQuery("p").css("font-size", currentFontSize + "px");
                        currentHeight = parseInt(jQuery("body").height());
                    }
                }
            """.trimIndent()

            view.loadUrl(javascript)
             view.post {
                progressBar.visibility = View.GONE
            }
            super.onPageFinished(view, url)
        }
    }

    inner class WebAppInterface(private val fullscreenActivity: FullscreenActivity) {

        /** Show a toast from the web page  */
        @JavascriptInterface
        fun showToast(toast: String) {
            Log.d(MY_FRAGMENT, toast)
            activity!!.runOnUiThread {
                // Stuff that updates the UI
                fullscreenActivity.toggle()
            }
        }

        @SuppressLint("InflateParams")
        @JavascriptInterface
        fun imgClick(source: String) {

            Log.d(MY_FRAGMENT, source)
            val settingsDialog = Dialog(
                requireContext(),
                android.R.style.Theme_Black_NoTitleBar_Fullscreen
            )
            settingsDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
            settingsDialog.setContentView(
                layoutInflater.inflate(
                    R.layout.image_full_screen
                    , null
                )
            )
            settingsDialog.setCancelable(true)
            val imageView: ImageView = settingsDialog.findViewById(R.id.imageView)
            val closeImg: ImageView = settingsDialog.findViewById(R.id.closeImg)
            imageView.setImageDrawable(getMyDrawable(source, webView.width))
            closeImg.setOnClickListener {
                settingsDialog.dismiss()
                requireActivity().runOnUiThread {
                    // Stuff that updates the UI
                    fullscreenActivity.toggle()
                }
            }
            settingsDialog.show()
        }

        fun getMyDrawable(source: String?, width: Int): Drawable {
            val inputStream = source?.let { requireActivity().assets.open(it) }
            val d = Drawable.createFromStream(inputStream, null)
            val height = (width * d.intrinsicHeight / d.intrinsicWidth)
            d.setBounds(0, 0, width, height)
            return d
        }

        @JavascriptInterface
        fun selectedTextMenu(type: String, startPos: String, endPos: String, selectedText: String) {
            Log.d(MY_FRAGMENT, "$type - $startPos - $endPos - $selectedText")
            val start = if ((startPos.toInt() - 20) < 0) 0 else (startPos.toInt() - 20)
            val end = start + selectedText.length + 10//endPos.toInt() + 20

            if (selectedText.isEmpty()) return
            if (start == end) return
            if (end < start) return
            if (end == 0) return
            if (start < 0) return

            when (type) {
                "save" -> {
//                    Toast.makeText(activity!!.applicationContext, type, Toast.LENGTH_SHORT).show()
                    saveToDataBase(selectedText, start, end, selectedColor)
                }

                "comment" -> {
//                    Toast.makeText(activity!!.applicationContext, type, Toast.LENGTH_SHORT).show()
                    commentText(start, end, selectedColorD, selectedText)
                }

                "copy" -> {
//                    Toast.makeText(activity!!.applicationContext, type, Toast.LENGTH_SHORT).show()
                    val myClipboard =
                        context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val myClip: ClipData = ClipData.newPlainText(
                        "note_copy",
                        selectedText
                    )
                    myClipboard.setPrimaryClip(myClip)
                }

                "share" -> {
//                    Toast.makeText(activity!!.applicationContext, type, Toast.LENGTH_SHORT).show()
                    val shareIntent = Intent()
                    shareIntent.action = Intent.ACTION_SEND
                    shareIntent.type = "text/plain"
                    shareIntent.putExtra(Intent.EXTRA_TEXT, selectedText)
                    startActivity(Intent.createChooser(shareIntent, null))
                }
            }


        }

//        @JavascriptInterface
//        fun resize(height: Float) {
//            val height0 = height
//            val webViewHeight = height * resources.displayMetrics.density
//            val height1 = webView.height
//            val myHeight = height1 / resources.displayMetrics.density
//            val i = 0;
//            //webViewHeight is the actual height of the WebView in pixels as per device screen density
//        }
    }

    @SuppressLint("SetTextI18n")
    private fun commentText(start: Int, end: Int, color: Int, text: String) {
        val dialogBuilder: AlertDialog.Builder =
            AlertDialog.Builder(requireActivity())
        val inflater = requireActivity().layoutInflater
        val dialogView: View = inflater.inflate(R.layout.comment_text, null)
        val editText = dialogView.findViewById<EditText>(R.id.commentTextComment)
        val save = dialogView.findViewById<Button>(R.id.saveComment)
        save.visibility = View.INVISIBLE
        val cancel = dialogView.findViewById<Button>(R.id.cancelComment)
        val chapter = dialogView.findViewById<TextView>(R.id.chapter)
        val pages = DataFromFile2.dataBaseAdapter.getOnePage(DataFromFile2.dataBaseAdapter.getCurrentPage())
        val chapters = DataFromFile2.dataBaseAdapter.getChapters()?.first { it.id == pages?.id_data }
        chapter.text = chapters?.title ?: ""
        val commentText = dialogView.findViewById<TextView>(R.id.commentText)
        commentText.text = """
            "$text"
        """.trimIndent()
        when (MyPref.getThemeMode()) {
            0 -> {
                dialogView.setBackgroundColor(requireActivity().resources.getColor(R.color.colorBackground))
                editText.setTextColor(requireActivity().resources.getColor(R.color.textColorPrimary))
            }
            1 -> {
                dialogView.setBackgroundColor(requireActivity().resources.getColor(R.color.colorBackgroundSepiy))
                editText.setTextColor(requireActivity().resources.getColor(R.color.textColorPrimarySepiy))
            }
            2 -> {
                dialogView.setBackgroundColor(requireActivity().resources.getColor(R.color.colorBackgroundNight))
                editText.setTextColor(requireActivity().resources.getColor(R.color.textColorPrimaryNight))
                save.setTextColor(Color.BLACK)
                cancel.setTextColor(Color.BLACK)
            }
            else -> {
                dialogView.setBackgroundColor(requireActivity().resources.getColor(R.color.colorBackground))
                editText.setTextColor(requireActivity().resources.getColor(R.color.textColorPrimary))
            }
        }
        dialogBuilder.setView(dialogView)
       // if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            editText.setOnFocusChangeListener { v, hasFocus ->
                editText.post {
                    val inputMethodManager =
                        requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    inputMethodManager.showSoftInput(
                        editText,
                        InputMethodManager.SHOW_IMPLICIT
                    )
                }
            }
        editText.requestFocus()
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.d("mylogs", s.toString())
                val text = s.toString().trim()
                if (text.isNotEmpty()) {
                    save.visibility = View.VISIBLE
                } else {
                    save.visibility = View.INVISIBLE
                }
            }

        })
        val alertDialog = dialogBuilder.create()
        alertDialog.setIcon(R.drawable.ic_comment_black_24dp)
        alertDialog.show()
        alertDialog.setCancelable(false)

        save.setOnClickListener {
            saveToDataBase(text, start, end, color, editText.text.toString())
            alertDialog.dismiss()

            requireActivity().runOnUiThread {
                // Stuff that updates the UI
                fullscreenActivity.mVisible = true
                fullscreenActivity.toggle()
            }
        }
        cancel.setOnClickListener {

            alertDialog.dismiss()
            requireActivity().runOnUiThread {
                fullscreenActivity.mVisible = true
                fullscreenActivity.toggle()
            }
        }
    }


    var selectedColor = Color.rgb(179, 229, 252)
    var selectedColorD = Color.rgb(220, 231, 117)
    private fun saveToDataBase(
        text: String,
        start: Int,
        end: Int,
        color: Int,
        description: String = ""
    ) {//
        val dataBaseAdapter = DataFromFile2.dataBaseAdapter
        val currentPage = dataBaseAdapter.getCurrentPage()
        val currentHtmlPage = dataBaseAdapter.getOnePage(currentPage) ?: return
        val spannable = HtmlToSpannable().getSpannableFromHtml(currentHtmlPage.pages)
        if (spannable.length < text.length) return
        val newStart =
            spannable.toString().indexOf(
                if (text.length > 20) {
                    val subtext = text.substring(0, 20)
                    subtext.trim()
                } else text.trim(), start
            )
        if (newStart == -1) return
        val newEnd = newStart + text.length

        spannable.setSpan(
            BackgroundColorSpan(color), newStart
            , newEnd,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        val spannableToHTmlConverter = MySpannedToHtmlWIthStyleClass()
        val newHtml = spannableToHTmlConverter.mySpannableConvertToHtmlWithStyle(
            MainApplication.applicationContext(),
            spannable,
            spannableToHTmlConverter.toHtmlParagraphLinesConsecutive
        )
        var jsoup = Jsoup.parse(newHtml)
        dataBaseAdapter.updatePages(currentHtmlPage.id, jsoup.html())
        dataBaseAdapter.setSelectedPages(
            favorite = 0,
            id_data = currentHtmlPage.id_data,
            start = newStart,
            end = newEnd,
            startMain = currentHtmlPage.start + newStart,
            endMain = currentHtmlPage.start + newEnd,
            page = currentPage,
            color = color,
            text = spannable.subSequence(newStart, newEnd).toString(),
            description = description
        )
        val htmlData = dataBaseAdapter.getOneHtmlData(currentHtmlPage.id_data)!!
        val mainSpannable = htmlData.spannable
        mainSpannable.setSpan(
            BackgroundColorSpan(color), currentHtmlPage.start + newStart
            , currentHtmlPage.start + newEnd,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        val mainHtml = spannableToHTmlConverter.mySpannableConvertToHtmlWithStyle(
            MainApplication.applicationContext(),
            mainSpannable,
            spannableToHTmlConverter.toHtmlParagraphLinesConsecutive
        )
        jsoup = Jsoup.parse(mainHtml)
        dataBaseAdapter.updateHtmlData(htmlData.id, jsoup.html())
        dataBaseAdapter.close()
        loadWeb(newHtml)
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun loadWeb(htmlText: String = "") {

        webView.post {

            progressBar.visibility = View.VISIBLE
            webView.webViewClient = MyWebClient()

            webView.settings.javaScriptEnabled = true
            webView.settings.domStorageEnabled = true
            webView.settings.loadWithOverviewMode = true
            webView.settings.useWideViewPort = true
            webView.settings.defaultTextEncodingName = "utf-8"
//            webView.settings.defaultFontSize = 25
//            webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING
            webView.webChromeClient = WebChromeClient()


            webView.addJavascriptInterface(WebAppInterface(fullscreenActivity), "Android")
            val document = Jsoup.parse(htmlText)
            document.getElementsByTag("html")[0].attr("lang", "ru")
            val head = document.head()
            val linkSyle = """
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            <link rel="stylesheet" href="htmlconfig/style.css">
        """.trimIndent()
            val linkScript = """
            <script type="text/javascript" src="htmlconfig/jquery.js"></script> 
            
        """.trimIndent()
            val linkMyScript = """
            <script type="text/javascript" src="htmlconfig/myscript.js"></script> 
        """.trimIndent()

            val customMenu = """
            <ul class='custom-menu noselect'>
             <!-- <li id="item-menu" style="font-size:12px" class="pallitre" data-action="pallitre">
                  <div><img src="htmlconfig/context/pallitre.png" /></div>
                  <div class="my-margin"></div>
                  <div>Цвет</div>
              </li> -->
              <li id="item-menu" style="font-size:12px" class="save" data-action="save">
                  <div><img src="htmlconfig/context/outline_save_black_18dp.png" /></div>
                  <div class="my-margin"></div>
                  <div>Сохран...</div>
              </li>
              <li id="item-menu" style="font-size:12px" class="comment" data-action="comment">
                  <div><img src="htmlconfig/context/outline_comment_black_18dp.png" /></div>
                  <div class="my-margin"></div>
                  <div>Коммен...</div>
              </li>
              <li id="item-menu" style="font-size:12px" class="copy" data-action="copy">
                  <div><img src="htmlconfig/context/outline_file_copy_black_18dp.png" /></div>
                  <div class="my-margin"></div>
                  <div>Копиро...</div>
              </li> 
              <li id="item-menu" style="font-size:12px" class="share" data-action="share">
                  <div><img src="htmlconfig/context/outline_share_black_18dp.png" /></div>
                  <div class="my-margin"></div>
                  <div>Подели...</div>
              </li>
            </ul>
            
        
        """.trimIndent()
//
            val fontSize = TextSize.textSizeListWeb[MyPref.getTextSize()]
            val fontSizeStyle = """
                <style>
                body{
                line-height: ${LineSpace.lineSpaceListWeb[MyPref.getLineSpace()]};
                font-family: '${TypeFace.typefaceList[MyPref.getTypeFace()].name}';
                color: ${when (MyPref.getThemeMode()) {
                        0 -> "#333333"
                        1 -> "#222222"
                        2 -> "#FAFAFA"
                        else -> "#333333"
                    }}
                }
                p{
                font-size: ${fontSize}px;
                text-align: justify;
                color: ${when (MyPref.getThemeMode()) {
                        0 -> "#333333"
                        1 -> "#222222"
                        2 -> "#FAFAFA"
                        else -> "#333333"
                    }}
                }
                ${TypeFace.fontFaceStyle}
                </style>
            """.trimIndent()

            document.body().append(fontSizeStyle)
            document.body().append(customMenu)
            head.append(linkSyle)
            head.append(linkScript)
            head.append(linkMyScript)
            Log.d(MY_FRAGMENT, document.html())

            webView.loadDataWithBaseURL(
                "file:///android_asset/",
                document.html(),
                "text/html",
                "utf-8",
                null
            )
        }

    }


}



