package com.kostakoz.srbook.srmvc.model.htmlcompat

import android.content.Context
import android.graphics.Typeface
import android.text.Layout
import android.text.Spanned
import android.text.TextUtils
import android.text.style.*

class MySpannedToHtmlWIthStyleClass {
    val toHtmlParagraphLinesConsecutive = 0x00000000
    private val toHtmlParagraphFlag = 0x00000001
    fun mySpannableConvertToHtmlWithStyle(
        context: Context,
        text: Spanned,
        option: Int
    ): String {
        val out = StringBuilder()
        withInMyHtml(context, out, text, option)
        return out.toString()
    }

    private fun withInMyHtml(
        context: Context,
        out: StringBuilder,
        text: Spanned,
        option: Int
    ) {
        if (option and toHtmlParagraphFlag == toHtmlParagraphLinesConsecutive) {
            encodeMyTextAlignmentByDiv(context, out, text, option)
            return
        }
        withInMyDiv(context, out, text, 0, text.length, option)
    }

    private fun encodeMyTextAlignmentByDiv(
        context: Context,
        out: StringBuilder,
        text: Spanned,
        option: Int
    ) {
        val len = text.length
        var next: Int
        var i = 0
        while (i < len) {
            next = text.nextSpanTransition(i, len, ParagraphStyle::class.java)
            val styles =
                text.getSpans(i, next, ParagraphStyle::class.java)
            var elements = " "
            var needDiv = false
            for (style in styles) {
                if (style is AlignmentSpan) {
                    val align =
                        style.alignment
                    needDiv = true
                    elements = if (align == Layout.Alignment.ALIGN_CENTER) {
                        "align=\"center\" $elements"
                    } else if (align == Layout.Alignment.ALIGN_OPPOSITE) {
                        "align=\"right\" $elements"
                    } else {
                        "align=\"left\" $elements"
                    }
                }
            }
            if (needDiv) {
                out.append("<div ").append(elements).append(">")
            }
            withInMyDiv(context, out, text, i, next, option)
            if (needDiv) {
                out.append("</div>")
            }
            i = next
        }
    }

    private fun withInMyDiv(
        context: Context, out: StringBuilder, text: Spanned,
        start: Int, end: Int, option: Int
    ) {
        var next: Int
        var i = start
        while (i < end) {
            next = text.nextSpanTransition(i, end, QuoteSpan::class.java)
            val quotes =
                text.getSpans(i, next, QuoteSpan::class.java)
            for (quote in quotes) {
                out.append("<blockquote>")
            }
            withInMyBlockquote(context, out, text, i, next, option)
            for (quote in quotes) {
                out.append("</blockquote>\n")
            }
            i = next
        }
    }

    private val myTextDirection: String
        private get() {
            val paraDir = Layout.DIR_LEFT_TO_RIGHT
            return when (paraDir) {
                Layout.DIR_RIGHT_TO_LEFT -> " dir=\"rtl\""
                Layout.DIR_LEFT_TO_RIGHT -> " dir=\"ltr\""
                else -> " dir=\"ltr\""
            }
        }

    private fun getMyTextStyles(
        text: Spanned, start: Int, end: Int,
        forceNoVerticalMargin: Boolean, includeTextAlign: Boolean
    ): String {
        var margin: String? = null
        var textAlign: String? = null
        if (forceNoVerticalMargin) {
            margin = "margin-top:0; margin-bottom:0;"
        }
        if (includeTextAlign) {
            val alignmentSpans =
                text.getSpans(start, end, AlignmentSpan::class.java)
            // Only use the last AlignmentSpan with flag SPAN_PARAGRAPH
            for (i in alignmentSpans.indices.reversed()) {
                val s = alignmentSpans[i]
                if (text.getSpanFlags(s) and Spanned.SPAN_PARAGRAPH == Spanned.SPAN_PARAGRAPH) {
                    val alignment = s.alignment
                    if (alignment == Layout.Alignment.ALIGN_NORMAL) {
                        textAlign = "text-align:start;"
                    } else if (alignment == Layout.Alignment.ALIGN_CENTER) {
                        textAlign = "text-align:center;"
                    } else if (alignment == Layout.Alignment.ALIGN_OPPOSITE) {
                        textAlign = "text-align:end;"
                    }
                    break
                }
            }
        }
        if (margin == null && textAlign == null) {
            return ""
        }
        val style = StringBuilder(" style=\"")
        if (margin != null && textAlign != null) {
            style.append(margin).append(" ").append(textAlign)
        } else if (margin != null) {
            style.append(margin)
        } else if (textAlign != null) {
            style.append(textAlign)
        }
        return style.append("\"").toString()
    }

    private fun withInMyBlockquote(
        context: Context, out: StringBuilder, text: Spanned,
        start: Int, end: Int, option: Int
    ) {
        if (option and toHtmlParagraphFlag == toHtmlParagraphLinesConsecutive) {
            withInMyBlockquoteConsecutive(
                context,
                out,
                text,
                start,
                end
            )
        } else {
            withInMyBlockquoteIndividual(
                context,
                out,
                text,
                start,
                end
            )
        }
    }

    private fun withInMyBlockquoteIndividual(
        context: Context, out: StringBuilder, text: Spanned,
        start: Int, end: Int
    ) {
        var isInList = false
        var next: Int
        var i = start
        while (i <= end) {
            next = TextUtils.indexOf(text, '\n', i, end)
            if (next < 0) {
                next = end
            }
            if (next == i) {
                if (isInList) {
                    // Current paragraph is no longer a list item; close the previously opened list
                    isInList = false
                    out.append("</ul>\n")
                }
                out.append("<br>\n")
            } else {
                var isListItem = false
                val paragraphStyles =
                    text.getSpans(i, next, ParagraphStyle::class.java)
                for (paragraphStyle in paragraphStyles) {
                    val spanFlags = text.getSpanFlags(paragraphStyle)
                    if (spanFlags and Spanned.SPAN_PARAGRAPH == Spanned.SPAN_PARAGRAPH
                        && paragraphStyle is BulletSpan
                    ) {
                        isListItem = true
                        break
                    }
                }
                if (isListItem && !isInList) {
                    // Current paragraph is the first item in a list
                    isInList = true
                    out.append("<ul")
                        .append(
                            getMyTextStyles(
                                text,
                                i,
                                next,
                                true,
                                false
                            )
                        )
                        .append(">\n")
                }
                if (isInList && !isListItem) {
                    // Current paragraph is no longer a list item; close the previously opened list
                    isInList = false
                    out.append("</ul>\n")
                }
                val tagType = if (isListItem) "li" else "p"
                out.append("<").append(tagType)
                    .append(myTextDirection)
                    .append(
                        getMyTextStyles(
                            text,
                            i,
                            next,
                            !isListItem,
                            true
                        )
                    )
                    .append(">")
                withInMyParagraph(context, out, text, i, next)
                out.append("</")
                out.append(tagType)
                out.append(">\n")
                if (next == end && isInList) {
                    isInList = false
                    out.append("</ul>\n")
                }
            }
            next++
            i = next
        }
    }

    private fun withInMyBlockquoteConsecutive(
        context: Context, out: StringBuilder, text: Spanned,
        start: Int, end: Int
    ) {
        out.append("<p").append(myTextDirection).append(">")
        var next: Int
        var i = start
        while (i < end) {
            next = TextUtils.indexOf(text, '\n', i, end)
            if (next < 0) {
                next = end
            }
            var nl = 0
            while (next < end && text[next] == '\n') {
                nl++
                next++
            }
            withInMyParagraph(context, out, text, i, next - nl)
            if (nl == 1) {
                out.append("<br>\n")
            } else {
                for (j in 2 until nl) {
                    out.append("<br>")
                }
                if (next != end) {
                    /* Paragraph should be closed and reopened */
                    out.append("</p>\n")
                    out.append("<p").append(myTextDirection)
                        .append(">")
                }
            }
            i = next
        }
        out.append("</p>\n")
    }

    private fun withInMyParagraph(
        context: Context,
        out: StringBuilder,
        text: Spanned,
        start: Int,
        end: Int
    ) {
        var next: Int
        var i = start
        while (i < end) {
            next = text.nextSpanTransition(i, end, CharacterStyle::class.java)
            val styles =
                text.getSpans(i, next, CharacterStyle::class.java)
            for (style in styles) {
                if (style is StyleSpan) {
                    val s = style.style
                    if (s and Typeface.BOLD != 0) {
                        out.append("<b>")
                    }
                    if (s and Typeface.ITALIC != 0) {
                        out.append("<i>")
                    }
                }
                if (style is TypefaceSpan) {
                    val s = style.family
                    if ("monospace" == s) {
                        out.append("<tt>")
                    }
                }
                if (style is SuperscriptSpan) {
                    out.append("<sup>")
                }
                if (style is SubscriptSpan) {
                    out.append("<sub>")
                }
                if (style is UnderlineSpan) {
                    out.append("<u>")
                }
                if (style is StrikethroughSpan) {
                    out.append("<span style=\"text-decoration:line-through;\">")
                }
                if (style is URLSpan) {
                    out.append("<a href=\"")
                    out.append(style.url)
                    out.append("\">")
                }
                if (style is ImageSpan) {
                    out.append("<img src=\"")
                    out.append(style.source)
                    out.append("\">")
                    // Don't output the dummy character underlying the image.
                    i = next
                }
                if (style is AbsoluteSizeSpan) {
                    val s = style
                    var sizeDip = s.size.toFloat()
                    if (!s.dip) {
                        sizeDip /= context.resources.displayMetrics.density
                    }
                    // px in CSS is the equivalance of dip in Android
                    //out.append(String.format("<span style=\"font-size:%.0fpx\";>", sizeDip))
                }
                if (style is RelativeSizeSpan) {
                    val sizeEm = style.sizeChange
                    //out.append(String.format("<span style=\"font-size:%.2fem;\">", sizeEm))
                }
                if (style is ForegroundColorSpan) {
                    val color = style.foregroundColor
                    out.append(
                        String.format(
                            "<span style=\"color:#%06X;\">",
                            0xFFFFFF and color
                        )
                    )
                }
                if (style is BackgroundColorSpan) {
                    val color = style.backgroundColor
                    out.append(
                        String.format(
                            "<span style=\"background-color:#%06X;\">",
                            0xFFFFFF and color
                        )
                    )
                }
            }
            withInMyStyle(out, text, i, next)
            for (j in styles.indices.reversed()) {
                if (styles[j] is BackgroundColorSpan) {
                    out.append("</span>")
                }
                if (styles[j] is ForegroundColorSpan) {
                    out.append("</span>")
                }
                if (styles[j] is RelativeSizeSpan) {
                    out.append("</span>")
                }
                if (styles[j] is AbsoluteSizeSpan) {
                    out.append("</span>")
                }
                if (styles[j] is URLSpan) {
                    out.append("</a>")
                }
                if (styles[j] is StrikethroughSpan) {
                    out.append("</span>")
                }
                if (styles[j] is UnderlineSpan) {
                    out.append("</u>")
                }
                if (styles[j] is SubscriptSpan) {
                    out.append("</sub>")
                }
                if (styles[j] is SuperscriptSpan) {
                    out.append("</sup>")
                }
                if (styles[j] is TypefaceSpan) {
                    val s = (styles[j] as TypefaceSpan).family
                    if (s == "monospace") {
                        out.append("</tt>")
                    }
                }
                if (styles[j] is StyleSpan) {
                    val s = (styles[j] as StyleSpan).style
                    if (s and Typeface.BOLD != 0) {
                        out.append("</b>")
                    }
                    if (s and Typeface.ITALIC != 0) {
                        out.append("</i>")
                    }
                }
            }
            i = next
        }
    }

    private fun withInMyStyle(
        out: StringBuilder, text: CharSequence,
        start: Int, end: Int
    ) {
        var i = start
        while (i < end) {
            val c = text[i]
            if (c == '<') {
                out.append("&lt;")
            } else if (c == '>') {
                out.append("&gt;")
            } else if (c == '&') {
                out.append("&amp;")
            } else if (c.toInt() in 0xD800..0xDFFF) {
                if (c.toInt() < 0xDC00 && i + 1 < end) {
                    val d = text[i + 1]
                    if (d.toInt() in 0xDC00..0xDFFF) {
                        i++
                        val codepoint =
                            0x010000 or (c.toInt() - 0xD800 shl 10) or d.toInt() - 0xDC00
                        out.append("&#").append(codepoint).append(";")
                    }
                }
            } else if (c.toInt() > 0x7E || c < ' ') {
                out.append("&#").append(c.toInt()).append(";")
            } else if (c == ' ') {
                while (i + 1 < end && text[i + 1] == ' ') {
                    out.append("&nbsp;")
                    i++
                }
                out.append(' ')
            } else {
                out.append(c)
            }
            i++
        }
    }
}