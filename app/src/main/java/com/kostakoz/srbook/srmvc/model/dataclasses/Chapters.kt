package com.kostakoz.srbook.srmvc.model.dataclasses

data class Chapters(
    val id: Int,
    val depth: Int,
    val title: String,
    val page: Int = 0
)