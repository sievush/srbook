package com.kostakoz.srbook.srmvc.model.dataclasses

class Pages (
    var id: Int,
    var id_data: Int,
    var pages: String,
    var start: Int,
    var end: Int,
    var page: Int,
    var text: String
)