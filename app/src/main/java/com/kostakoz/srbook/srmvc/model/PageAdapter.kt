package com.kostakoz.srbook.srmvc.model

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.kostakoz.srbook.srmvc.model.MyFragment
import com.kostakoz.srbook.srmvc.model.dataclasses.Pages


class PageAdapter(fragmentActivity: FragmentManager, lifecycle: Lifecycle, private val size: Int) :
    FragmentStateAdapter(fragmentActivity, lifecycle) {
    override fun getItemCount(): Int = size

    override fun createFragment(position: Int): Fragment {
        return MyFragment().newInstance(position)
    }

}