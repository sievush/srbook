package com.kostakoz.srbook.srmvc.model

import com.kostakoz.srbook.MainApplication
import com.kostakoz.srbook.R

class LeftRightMargins {
    companion object {
        private val context = MainApplication.applicationContext()
        var leftRightMarginsList = listOf(
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins0),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins1),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins2),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins3),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins4),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins5),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins6),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins7),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins8),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins9),
            context.resources.getDimensionPixelSize(R.dimen.right_left_margins10)
        )
    }
}