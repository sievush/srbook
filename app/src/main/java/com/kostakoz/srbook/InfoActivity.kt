package com.kostakoz.srbook

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import kotlinx.android.synthetic.main.activity_info.*

class InfoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        MyPref.themeAndOrientationConfig(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = DataFromFile2.dataBaseAdapter.getTitle()
        toolbar.title = DataFromFile2.dataBaseAdapter.getTitle()
        if (MyPref.getThemeMode() == 2) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
            toolbar.setTitleTextColor(resources.getColor(R.color.textColorPrimaryNight))
        }else{
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp2)
        }
        toolbar.setNavigationOnClickListener {
            finish()
        }

        bookName.text = DataFromFile2.dataBaseAdapter.getTitle()
        bookDate.text = DataFromFile2.dataBaseAdapter.getDate()

        val builder = StringBuilder()
        if (DataFromFile2.dataBaseAdapter.getAuthors() != null) {
            DataFromFile2.dataBaseAdapter.getAuthors()!!.forEach {
                builder.append(it.firstname + " " + it.lastname)
                builder.append(",")
                builder.append("\n")
            }
        }
        bookAuthors.text = builder.toString()

        bookDescription.text = DataFromFile2.dataBaseAdapter.getDescriptions()

        val builder2 = StringBuilder()
        if (DataFromFile2.dataBaseAdapter.getSubjects() != null) {
            DataFromFile2.dataBaseAdapter.getSubjects()!!.forEach {
                builder2.append(it)
                builder2.append(",")
                builder2.append("\n")
            }
        }

        bookGenre.text = builder2.toString()
    }
}
