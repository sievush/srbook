package com.kostakoz.srbook.preferences

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import com.kostakoz.srbook.MainApplication
import com.kostakoz.srbook.R

object MyPref {
    private val context = MainApplication.applicationContext()
    private val defaultValue = context.resources.getInteger(R.integer.saved_high_score_default_key)

    fun clearAll(){
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_theme_file_key), Context.MODE_PRIVATE
        )
        val sharedPref2 = context.getSharedPreferences(
            context.getString(R.string.preference__text_size_file_key), Context.MODE_PRIVATE
        )
        val sharedPref3 = context.getSharedPreferences(
            context.getString(R.string.preference_type_face_file_key), Context.MODE_PRIVATE
        )
        val sharedPref4 = context.getSharedPreferences(
            context.getString(R.string.preference_orientation_file_key), Context.MODE_PRIVATE
        )
        val sharedPref5 = context.getSharedPreferences(
            context.getString(R.string.preference_animation_swipe_file_key), Context.MODE_PRIVATE
        )
        val sharedPref6 = context.getSharedPreferences(
            context.getString(R.string.preference_line_space_file_key), Context.MODE_PRIVATE
        )
        val sharedPref7 = context.getSharedPreferences(
            context.getString(R.string.preference_right_left_margins_file_key), Context.MODE_PRIVATE
        )
        with(sharedPref.edit()){
            clear()
            commit()
        }
        with(sharedPref2.edit()){
            clear()
            commit()
        }
        with(sharedPref3.edit()){
            clear()
            commit()
        }
        with(sharedPref4.edit()){
            clear()
            commit()
        }
        with(sharedPref5.edit()){
            clear()
            commit()
        }
        with(sharedPref6.edit()){
            clear()
            commit()
        }
        with(sharedPref7.edit()){
            clear()
            commit()
        }
    }

    fun setThemeMode(current: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_theme_file_key), Context.MODE_PRIVATE
        )
        with(sharedPref.edit()) {
            putInt(context.getString(R.string.current_theme_mode), current)
            commit()
        }
    }

    fun getThemeMode(): Int {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_theme_file_key), Context.MODE_PRIVATE
        )
        return sharedPref.getInt(context.getString(R.string.current_theme_mode), defaultValue)
    }

    fun setTextSize(current: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference__text_size_file_key), Context.MODE_PRIVATE
        )
        with(sharedPref.edit()) {
            putInt(context.getString(R.string.current_text_size), current)
            commit()
        }
    }

    fun getTextSize(): Int {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference__text_size_file_key), Context.MODE_PRIVATE
        )
        return sharedPref.getInt(context.getString(R.string.current_text_size), 1)
    }

    fun setTypeFace(current: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_type_face_file_key), Context.MODE_PRIVATE
        )
        with(sharedPref.edit()) {
            putInt(context.getString(R.string.current_type_face), current)
            commit()
        }
    }

    fun getTypeFace(): Int {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_type_face_file_key), Context.MODE_PRIVATE
        )
        return sharedPref.getInt(context.getString(R.string.current_type_face), defaultValue)
    }

    fun setOrientationScreen(current: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_orientation_file_key), Context.MODE_PRIVATE
        )
        with(sharedPref.edit()) {
            putInt(context.getString(R.string.current_orientation), current)
            commit()
        }
    }

    fun getOrientationScreen(): Int {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_orientation_file_key), Context.MODE_PRIVATE
        )
        return sharedPref.getInt(context.getString(R.string.current_orientation), defaultValue)
    }

    fun setAnimationSwipePage(current: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_animation_swipe_file_key), Context.MODE_PRIVATE
        )
        with(sharedPref.edit()) {
            putInt(context.getString(R.string.current_animation_swipe), current)
            commit()
        }
    }

    fun getAnimationSwipePage(): Int {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_animation_swipe_file_key), Context.MODE_PRIVATE
        )
        return sharedPref.getInt(context.getString(R.string.current_animation_swipe), defaultValue)
    }

    fun setLineSpace(current: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_line_space_file_key), Context.MODE_PRIVATE
        )
        with(sharedPref.edit()) {
            putInt(context.getString(R.string.current_line_space), current)
            commit()
        }
    }

    fun getLineSpace(): Int {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_line_space_file_key), Context.MODE_PRIVATE
        )
        return sharedPref.getInt(context.getString(R.string.current_line_space), 4)
    }

    fun setRightLeftMargins(current: Int) {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_right_left_margins_file_key), Context.MODE_PRIVATE
        )
        with(sharedPref.edit()) {
            putInt(context.getString(R.string.current_right_left_margins), current)
            commit()
        }
    }

    fun getRightLeftMargins(): Int {
        val sharedPref = context.getSharedPreferences(
            context.getString(R.string.preference_right_left_margins_file_key), Context.MODE_PRIVATE
        )
        return sharedPref.getInt(context.getString(R.string.current_right_left_margins), 1)
    }

    @SuppressLint("SourceLockedOrientationActivity")
    fun themeAndOrientationConfig(activity: AppCompatActivity){
        when (getThemeMode()) {
            0 -> activity.setTheme(R.style.DayStyle)
            1 -> activity.setTheme(R.style.SepiyStyle)
            2 -> activity.setTheme(R.style.NightStyle)
            else -> activity.setTheme(R.style.DayStyle)
        }
        when (getOrientationScreen()) {
            1 -> activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            2 -> activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        }
    }

}