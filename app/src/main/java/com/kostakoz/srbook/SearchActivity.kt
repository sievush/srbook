package com.kostakoz.srbook

import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.widget.SearchView
import android.widget.SearchView.OnQueryTextListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import com.kostakoz.srbook.srmvc.model.HtmlToSpannable
import com.kostakoz.srbook.srmvc.model.SearchAdapter
import com.kostakoz.srbook.srmvc.model.dataclasses.Pages
import kotlinx.android.synthetic.main.activity_search.*

private const val TAG = "searchActivity"

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        MyPref.themeAndOrientationConfig(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
        toolbar.title = ""
        if (MyPref.getThemeMode() == 2) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
        } else {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp2)
        }
        toolbar.setNavigationOnClickListener {
            goBack()
        }
//        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
//            if(hasFocus){
//                val inputMethodManager =
//                    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//                inputMethodManager.showSoftInput(
//                    v,
//                    InputMethodManager.SHOW_IMPLICIT
//                )
//            }
//        }
//        searchView.requestFocus()
        searchView.clearFocus()
        searchView.queryHint = "Search Name"
//        searchView.onActionViewExpanded()
        searchView.isIconified = false
        searchView.requestFocus()

        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.itemAnimator = DefaultItemAnimator()


        searchView.setOnQueryTextListener(object : OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
//                Log.d(TAG, query)
                val list = DataFromFile2.pages!!.filter { HtmlToSpannable().getSpannableFromHtml(it.pages).toString().indexOf(query!!.trim()) > -1} as ArrayList<Pages>
                val adapter = SearchAdapter(list, query!!.trim())
                recyclerView.adapter = adapter
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
//                Log.d(TAG, newText)
                return true
            }

        })
    }

    private fun goBack(){
        startActivity(Intent(this, FullscreenActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        finish()
    }

    override fun onBackPressed() {
        goBack()
    }


}
