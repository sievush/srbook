package com.kostakoz.srbook.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.kostakoz.srbook.ChaptersActivity
import com.kostakoz.srbook.FullscreenActivity
import com.kostakoz.srbook.R
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import com.kostakoz.srbook.srmvc.model.dataclasses.Chapters
import com.kostakoz.srbook.srmvc.model.savedata.DataBaseAdapter
import kotlinx.android.synthetic.main.activity_chapters.*
import java.util.*


private const val TAG = "customAdapter"

class CustomAdapterChapters(
    private val chaptersList: ArrayList<Chapters>,
    private val dataBaseAdapter: DataBaseAdapter
) :
    RecyclerView.Adapter<CustomAdapterChapters.MyViewHolder>() {

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var textViewName: TextView = itemView.findViewById<View>(R.id.textViewName) as TextView
        var textViewVersion: TextView =
            itemView.findViewById<View>(R.id.textViewVersion) as TextView
        var cardView: CardView = itemView.findViewById(R.id.cardView) as CardView

    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cards_layout_chap, parent, false)
        view.setBackgroundColor(Color.TRANSPARENT)
        return MyViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        val chapters = chaptersList[listPosition]
        val textViewName = holder.textViewName
        val textViewVersion = holder.textViewVersion
        val pages = DataFromFile2.pages!!.first { it.id_data == chapters.id }
        val currentPage = dataBaseAdapter.getOnePage(dataBaseAdapter.getCurrentPage())
        textViewName.text = chapters.title//"""${chapters.title}........................"""
        textViewVersion.text = """${pages.page + 1} стр"""
        var leftPadding = 15
        for (i in 0..chapters.depth) {
            leftPadding += 50
        }
        if (chapters.depth == 0) {
            leftPadding = 15
        }

        if (currentPage != null) {
            if (currentPage.id_data.minus(1) == listPosition) {
                textViewName.setTextColor(Color.parseColor("#E65100"))
                textViewVersion.setTextColor(Color.parseColor("#E65100"))
            }
        }
        val margins = (holder.cardView.layoutParams as RecyclerView.LayoutParams).apply {
//            leftMargin = 10
//            rightMargin = 10
            holder.cardView.setContentPadding(leftPadding, 0, 0, 0)
        }
        holder.cardView.layoutParams = margins

        holder.cardView.setOnClickListener {
            Handler().postDelayed({
                // This method will be executed once the timer is over
                // Start your app main activity

                val context = holder.cardView.context

                pages.page.let { it1 -> dataBaseAdapter.updateCurrentPage(it1) }
                val activity = context as ChaptersActivity
                activity.progressBar.visibility = View.VISIBLE

                if (activity.isChangeMainProperties) {
                    activity.startActivity(
                        Intent(
                            activity,
                            FullscreenActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )
                    activity.finish()
                } else activity.finish()
            }, 1)
        }
    }

    override fun getItemCount(): Int {
        return chaptersList.size
    }

}