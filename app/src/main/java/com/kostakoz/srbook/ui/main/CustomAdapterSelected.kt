package com.kostakoz.srbook.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Handler
import android.text.Html
import android.text.style.BackgroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.kostakoz.srbook.ChaptersActivity
import com.kostakoz.srbook.FullscreenActivity
import com.kostakoz.srbook.R
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import com.kostakoz.srbook.srmvc.model.HtmlToSpannable
import com.kostakoz.srbook.srmvc.model.dataclasses.Selected
import com.kostakoz.srbook.srmvc.model.savedata.DataBaseAdapter
import kotlinx.android.synthetic.main.activity_chapters.*
import org.jsoup.Jsoup
import java.util.*


private const val TAG = "customAdapter"

class CustomAdapterSelected(
    private val whatTab: Int,
    private val fragment: Fragment,
    private val selectedList: ArrayList<Selected>,
    private val dataBaseAdapter: DataBaseAdapter
) :
    RecyclerView.Adapter<CustomAdapterSelected.MyViewHolder>() {

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var textViewChapter: TextView =
            itemView.findViewById<View>(R.id.textViewChapter) as TextView
        var textViewSelectText: TextView =
            itemView.findViewById<View>(R.id.textViewSelectText) as TextView

        var textViewPage: TextView = itemView.findViewById<View>(R.id.textViewPage) as TextView
        var elseMore: ImageView = itemView.findViewById<View>(R.id.elseMore) as ImageView
        val goToPage: LinearLayout = itemView.findViewById(R.id.goToPage) as LinearLayout
        val leftBackground: LinearLayout = itemView.findViewById(R.id.leftBackground) as LinearLayout
        var cardView: CardView = itemView.findViewById(R.id.cardView) as CardView
        var view = itemView as View
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cards_layout_sel, parent, false)
//        view.setBackgroundColor(Color.TRANSPARENT)
        return MyViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        when(MyPref.getThemeMode()){
            0 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.colorBackground))
            1 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.cardviewBackground2))
            2 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.cardviewBackground))
            else -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.colorBackground))
        }
        val selected = selectedList[selectedList.size - 1 - listPosition]
        val chapterTextView = holder.textViewChapter
        val textViewSelectText = holder.textViewSelectText
        val textViewPage = holder.textViewPage
        val leftBackground = holder.leftBackground
        val chapters = dataBaseAdapter.getChapters()!!.first { it.id == selected.id_data }

        val context = holder.cardView.context
        val activity = context as ChaptersActivity

        chapterTextView.text = chapters.title
        textViewSelectText.text = selected.text

        textViewPage.text = """${selected.page + 1} стр""".trimIndent()

        leftBackground.setBackgroundColor(selected.color)

        val elseMore = holder.elseMore
        elseMore.setOnClickListener {
            val popupMenu: PopupMenu = PopupMenu(holder.view.context, elseMore)
            popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)

            popupMenu.menu.findItem(R.id.action_edit).isVisible = false

            popupMenu.menu.findItem(R.id.action_edit).isEnabled = false

            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.action_share ->
                    {
                        val message: String = """
                            Оглавление: ${chapters.title} 
                            
                            Текст: "${selected.text}"
                        """.trimIndent()
                        val shareIntent = Intent()
                        shareIntent.action = Intent.ACTION_SEND
                        shareIntent.type = "text/plain"
                        shareIntent.putExtra(Intent.EXTRA_TEXT, message)
                        holder.view.context.startActivity(Intent.createChooser(shareIntent, null))
                    }
//                        Toast.makeText(
//                            holder.view.context,
//                            """You Clicked : ${item.title} - ${selected.id}""",
//                            Toast.LENGTH_SHORT
//                        ).show()
                    R.id.action_remove -> {
                        Toast.makeText(
                            holder.view.context,
                            """You Clicked : ${item.title} - ${selected.id}""",
                            Toast.LENGTH_SHORT
                        ).show()
                        activity.isChangeMainProperties = true
                        val pages = dataBaseAdapter.getOnePage(selected.page)!!
                        val spannable = HtmlToSpannable().getSpannableFromHtml(pages.pages)
                        val htmlData = dataBaseAdapter.getOneHtmlData(pages.id_data)!!
                        val mainSpannable = htmlData.spannable
                        val backgroundColorSpan = spannable.getSpans(
                            selected.start,
                            selected.end,
                            BackgroundColorSpan::class.java
                        )
                        for (span in backgroundColorSpan) {
                            spannable.removeSpan(span)
                        }
                        val backgroundColorSpanMain = mainSpannable.getSpans(
                            selected.startMain,
                            selected.endMain,
                            BackgroundColorSpan::class.java
                        )
                        for (span in backgroundColorSpanMain) {
                            mainSpannable.removeSpan(span)
                        }

                        var jsoup = Jsoup.parse(Html.toHtml(spannable))
                        dataBaseAdapter.updatePages(pages.id, jsoup.html())
                        jsoup = Jsoup.parse(Html.toHtml(mainSpannable))
                        dataBaseAdapter.updateHtmlData(htmlData.id, jsoup.html())
                        dataBaseAdapter.deleteSelectedPages(selected.id)
                        val favFragment = fragment as ChaptersFragment
                        favFragment.update(whatTab)
                    }

                }
                true
            }
            popupMenu.show()
        }

        val goToPage = holder.goToPage
        goToPage.setOnClickListener {
            Handler().postDelayed({
                dataBaseAdapter.updateCurrentPage(selected.page)
                activity.progressBar.visibility = View.VISIBLE

                if (activity.isChangeMainProperties) {
                    activity.startActivity(
                        Intent(
                            activity,
                            FullscreenActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )
                    activity.finish()
                } else activity.finish()
            }, 1)
        }

        holder.cardView.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return selectedList.size
    }

}