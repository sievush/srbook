package com.kostakoz.srbook.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel

class PageViewModel : ViewModel() {

    private val _index = MutableLiveData<Int>()
//    val text: LiveData<String> = Transformations.map(_index) {
//        "Hello world from section: $it"
//    }
//
//    val chaptersList: LiveData<Chapters> = Transformations.map(_index){
//        return@map DataFromFile2.chaptersList[it]
//    }

    val whatTabs: LiveData<Int> = Transformations.map(_index){
        return@map it
    }

    fun setIndex(index: Int) {
        _index.value = index
    }
}