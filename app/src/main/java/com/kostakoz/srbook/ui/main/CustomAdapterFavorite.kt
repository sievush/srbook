package com.kostakoz.srbook.ui.main

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.PopupMenu
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.kostakoz.srbook.ChaptersActivity
import com.kostakoz.srbook.FullscreenActivity
import com.kostakoz.srbook.R
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.dataclasses.Selected
import com.kostakoz.srbook.srmvc.model.savedata.DataBaseAdapter
import kotlinx.android.synthetic.main.activity_chapters.*
import java.util.*


private const val TAG = "customAdapter"

class CustomAdapterFavorite(
    private val whatTab: Int,
    private val fragment: Fragment,
    private val selectedList: ArrayList<Selected>,
    private val dataBaseAdapter: DataBaseAdapter
) :
    RecyclerView.Adapter<CustomAdapterFavorite.MyViewHolder>() {

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var textViewChapter: TextView =
            itemView.findViewById<View>(R.id.textViewChapter) as TextView
        var textViewText: TextView = itemView.findViewById<View>(R.id.textViewText) as TextView
        var textViewPage: TextView = itemView.findViewById<View>(R.id.textViewPage) as TextView
        var elseMore: ImageView = itemView.findViewById<View>(R.id.elseMore) as ImageView
        val goToPage: LinearLayout = itemView.findViewById(R.id.goToPage) as LinearLayout
        var cardView: CardView = itemView.findViewById(R.id.cardView) as CardView
        var view = itemView as View

    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cards_layout_fav, parent, false)
//        view.setBackgroundColor(Color.TRANSPARENT)
        return MyViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        when(MyPref.getThemeMode()){
            0 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.colorBackground))
            1 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.cardviewBackground2))
            2 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.cardviewBackground))
            else -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.colorBackground))
        }
        val selected = selectedList[selectedList.size - 1 - listPosition]
        val chapterTextView = holder.textViewChapter
        val textViewText = holder.textViewText
        val textViewPage = holder.textViewPage

        val chapters = dataBaseAdapter.getChapters()!!.first { it.id == selected.id_data }

        chapterTextView.text = chapters.title
        textViewText.text = selected.text
        textViewPage.text = """${selected.page + 1} стр""".trimIndent()

        val elseMore = holder.elseMore

        elseMore.setOnClickListener {
            val popupMenu: PopupMenu = PopupMenu(holder.view.context, elseMore)
            popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)

            popupMenu.menu.findItem(R.id.action_share).isVisible = false
            popupMenu.menu.findItem(R.id.action_edit).isVisible = false

            popupMenu.menu.findItem(R.id.action_share).isEnabled = false
            popupMenu.menu.findItem(R.id.action_edit).isEnabled = false

            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.action_remove -> {
                        Toast.makeText(
                            holder.view.context,
                            """You Clicked : ${item.title} - ${selected.id}""",
                            Toast.LENGTH_SHORT
                        ).show()
                        dataBaseAdapter.deleteSelectedPages(selected.id)
                        val favFragment = fragment as ChaptersFragment
                        favFragment.update(whatTab)
                    }

                }
                true
            }
            popupMenu.show()
        }


        val goToPage = holder.goToPage
        goToPage.setOnClickListener {
            Handler().postDelayed({
                val context = holder.cardView.context
                dataBaseAdapter.updateCurrentPage(selected.page)
                val activity = context as ChaptersActivity
                activity.progressBar.visibility = View.VISIBLE

                if (activity.isChangeMainProperties) {
                    activity.startActivity(
                        Intent(
                            activity,
                            FullscreenActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )
                    activity.finish()
                } else activity.finish()
            }, 1)
        }

        holder.cardView.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return selectedList.size
    }

}