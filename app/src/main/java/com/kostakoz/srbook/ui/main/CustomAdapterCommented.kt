package com.kostakoz.srbook.ui.main

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Handler
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.text.style.BackgroundColorSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.kostakoz.srbook.ChaptersActivity
import com.kostakoz.srbook.FullscreenActivity
import com.kostakoz.srbook.R
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import com.kostakoz.srbook.srmvc.model.HtmlToSpannable
import com.kostakoz.srbook.srmvc.model.dataclasses.Selected
import com.kostakoz.srbook.srmvc.model.savedata.DataBaseAdapter
import kotlinx.android.synthetic.main.activity_chapters.*
import org.jsoup.Jsoup
import java.util.*


private const val TAG = "customAdapter"

class CustomAdapterCommented(
    private val activity: Activity,
    private val whatTab: Int,
    private val fragment: Fragment,
    private val selectedList: ArrayList<Selected>,
    private val dataBaseAdapter: DataBaseAdapter
) :
    RecyclerView.Adapter<CustomAdapterCommented.MyViewHolder>() {

    private lateinit var textViewComment: TextView
    private var description = ""
    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var textViewChapter: TextView =
            itemView.findViewById<View>(R.id.textViewChapter) as TextView
        var textViewSelectText: TextView =
            itemView.findViewById<View>(R.id.textViewSelectText) as TextView
        var textViewComment: TextView =
            itemView.findViewById<View>(R.id.textViewComment) as TextView
        var textViewPage: TextView = itemView.findViewById<View>(R.id.textViewPage) as TextView
        var elseMore: ImageView = itemView.findViewById<View>(R.id.elseMore) as ImageView
        val goToPage: LinearLayout = itemView.findViewById(R.id.goToPage) as LinearLayout
        val leftBackground: LinearLayout = itemView.findViewById(R.id.leftBackground) as LinearLayout
        var cardView: CardView = itemView.findViewById(R.id.cardView) as CardView
        var view = itemView
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.cards_layout_comm, parent, false)
//        view.setBackgroundColor(Color.TRANSPARENT)
        return MyViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        when(MyPref.getThemeMode()){
            0 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.colorBackground))
            1 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.cardviewBackground2))
            2 -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.cardviewBackground))
            else -> holder.cardView.setCardBackgroundColor(holder.view.resources.getColor(R.color.colorBackground))
        }
        textViewComment = holder.textViewComment
        val selected = selectedList[selectedList.size - 1 - listPosition]
        val chapterTextView = holder.textViewChapter
        val textViewSelectText = holder.textViewSelectText
        val textViewComment = holder.textViewComment
        val textViewPage = holder.textViewPage
        val leftBackground = holder.leftBackground

        val chapters = dataBaseAdapter.getChapters()!!.first { it.id == selected.id_data }

        val context = holder.cardView.context
        val activity = context as ChaptersActivity

        chapterTextView.text = chapters.title
        textViewSelectText.text = selected.text
        textViewComment.text = selected.description
        textViewPage.text = """${selected.page + 1} стр""".trimIndent()

        description = selected.description

        leftBackground.setBackgroundColor(selected.color)

        val elseMore = holder.elseMore
        elseMore.setOnClickListener {
            val popupMenu: PopupMenu = PopupMenu(holder.view.context, elseMore)
            popupMenu.menuInflater.inflate(R.menu.popup_menu, popupMenu.menu)

            popupMenu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.action_share -> {
                        val message: String = """
                            Оглавление: ${chapters.title} 
                            
                            Текст: "${selected.text}"
                            
                            Комментарии: "$description"
                        """.trimIndent()
                        val shareIntent = Intent()
                        shareIntent.action = Intent.ACTION_SEND
                        shareIntent.type = "text/plain"
                        shareIntent.putExtra(Intent.EXTRA_TEXT, message)
                        holder.view.context.startActivity(Intent.createChooser(shareIntent, null))
                    }
//                        Toast.makeText(
//                            holder.view.context,
//                            """You Clicked : ${item.title} - ${selected.id}""",
//                            Toast.LENGTH_SHORT
//                        ).show()
                    R.id.action_edit -> {
                        commentText(selected.id, selected.page, selected.text, selected.description)
                    }
//                        Toast.makeText(
//                            holder.view.context,
//                            """You Clicked : ${item.title} - ${selected.id}""",
//                            Toast.LENGTH_SHORT
//                        ).show()
                    R.id.action_remove -> {
                        Toast.makeText(
                            holder.view.context,
                            """You Clicked : ${item.title} - ${selected.id}""",
                            Toast.LENGTH_SHORT
                        ).show()
                        activity.isChangeMainProperties = true
                        val pages = dataBaseAdapter.getOnePage(selected.page)!!
                        val spannable = HtmlToSpannable().getSpannableFromHtml(pages.pages)
                        val htmlData = dataBaseAdapter.getOneHtmlData(pages.id_data)!!
                        val mainSpannable = htmlData.spannable
                        val backgroundColorSpan = spannable.getSpans(
                            selected.start,
                            selected.end,
                            BackgroundColorSpan::class.java
                        )
                        for (span in backgroundColorSpan) {
                            spannable.removeSpan(span)
                        }
                        val backgroundColorSpanMain = mainSpannable.getSpans(
                            selected.startMain,
                            selected.endMain,
                            BackgroundColorSpan::class.java
                        )
                        for (span in backgroundColorSpanMain) {
                            mainSpannable.removeSpan(span)
                        }

                        var jsoup = Jsoup.parse(Html.toHtml(spannable))
                        dataBaseAdapter.updatePages(pages.id, jsoup.html())
                        jsoup = Jsoup.parse(Html.toHtml(mainSpannable))
                        dataBaseAdapter.updateHtmlData(htmlData.id, jsoup.html())
                        dataBaseAdapter.deleteSelectedPages(selected.id)
                        val favFragment = fragment as ChaptersFragment
                        favFragment.update(whatTab)
                    }


                }
                true
            }
            popupMenu.show()
        }

        val goToPage = holder.goToPage
        goToPage.setOnClickListener {
            Handler().postDelayed({
                dataBaseAdapter.updateCurrentPage(selected.page)
                activity.progressBar.visibility = View.VISIBLE

                if (activity.isChangeMainProperties) {
                    activity.startActivity(
                        Intent(
                            activity,
                            FullscreenActivity::class.java
                        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    )
                    activity.finish()
                } else activity.finish()
            }, 1)
        }

        holder.cardView.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return selectedList.size
    }


    @SuppressLint("SetTextI18n")
    private fun commentText(id: Int, page: Int, text: String, description: String) {
        val dialogBuilder: AlertDialog.Builder =
            AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater
        val dialogView: View = inflater.inflate(R.layout.comment_text, null)
        val editText = dialogView.findViewById<EditText>(R.id.commentTextComment)
        val save = dialogView.findViewById<Button>(R.id.saveComment)
        save.visibility = View.INVISIBLE
        val cancel = dialogView.findViewById<Button>(R.id.cancelComment)
        val chapter = dialogView.findViewById<TextView>(R.id.chapter)
        val pages = DataFromFile2.dataBaseAdapter.getOnePage(page)
        val chapters =
            DataFromFile2.dataBaseAdapter.getChapters()?.first { it.id == pages?.id_data }
        chapter.text = chapters?.title ?: ""
        val commentText = dialogView.findViewById<TextView>(R.id.commentText)
        commentText.text = """
            "$text"
        """.trimIndent()
        when (MyPref.getThemeMode()) {
            0 -> {
                dialogView.setBackgroundColor(activity.resources.getColor(R.color.colorBackground))
                editText.setTextColor(activity.resources.getColor(R.color.textColorPrimary))
            }
            1 -> {
                dialogView.setBackgroundColor(activity.resources.getColor(R.color.colorBackgroundSepiy))
                editText.setTextColor(activity.resources.getColor(R.color.textColorPrimarySepiy))
            }
            2 -> {
                dialogView.setBackgroundColor(activity.resources.getColor(R.color.colorBackgroundNight))
                editText.setTextColor(activity.resources.getColor(R.color.textColorPrimaryNight))
                save.setTextColor(Color.BLACK)
                cancel.setTextColor(Color.BLACK)
            }
            else -> {
                dialogView.setBackgroundColor(activity.resources.getColor(R.color.colorBackground))
                editText.setTextColor(activity.resources.getColor(R.color.textColorPrimary))
            }
        }
        dialogBuilder.setView(dialogView)
        // if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
        editText.setOnFocusChangeListener { v, hasFocus ->
            editText.post {
                val inputMethodManager =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.showSoftInput(
                    editText,
                    InputMethodManager.SHOW_IMPLICIT
                )
            }
        }
        editText.requestFocus()
        editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.d("mylogs", s.toString())
                val text = s.toString().trim()
                if (text.isNotEmpty()) {
                    save.visibility = View.VISIBLE
                } else {
                    save.visibility = View.INVISIBLE
                }
            }

        })
        editText.setText(description)
        val alertDialog = dialogBuilder.create()
        alertDialog.setIcon(R.drawable.ic_comment_black_24dp)
        alertDialog.show()
        alertDialog.setCancelable(false)

        save.setOnClickListener {
            DataFromFile2.dataBaseAdapter.updateSelectedDescription(id, editText.text.toString())
            textViewComment.text = editText.text
            this.description = editText.text.toString()
            alertDialog.dismiss()
        }
        cancel.setOnClickListener {
            alertDialog.dismiss()
        }
    }

}