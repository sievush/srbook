package com.kostakoz.srbook.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kostakoz.srbook.R
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import com.kostakoz.srbook.srmvc.model.dataclasses.Selected
import com.kostakoz.srbook.srmvc.model.savedata.DataBaseAdapter
import java.util.ArrayList

/**
 * A placeholder fragment containing a simple view.
 */
class ChaptersFragment : Fragment() {
    private lateinit var pageViewModel: PageViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var myView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        when (MyPref.getThemeMode()) {
            0 -> requireActivity().setTheme(R.style.DayStyle)
            1 -> requireActivity().setTheme(R.style.SepiyStyle)
            2 -> requireActivity().setTheme(R.style.NightStyle)
            else -> requireActivity().setTheme(R.style.DayStyle)
        }
        super.onCreate(savedInstanceState)
        pageViewModel = ViewModelProviders.of(this).get(PageViewModel::class.java).apply {
            setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 1)
        }
    }

    private fun createUpdateMyView(position: Int, dataBaseAdapter: DataBaseAdapter) {

        when (position) {
            0 -> {
                val chaptersList = dataBaseAdapter.getChapters()!!
                val chapAdapter =
                    CustomAdapterChapters(
                        chaptersList,
                        dataBaseAdapter
                    )
                recyclerView.adapter = chapAdapter
                val currentPage = dataBaseAdapter.getOnePage(dataBaseAdapter.getCurrentPage())
                if (currentPage != null) {
                    recyclerView.scrollToPosition(currentPage.id_data.minus(1))
                }
            }

            1 -> {
                val favList =
                    dataBaseAdapter.getSelectedPages()?.filter { it1 -> it1.favorite == 1 }
                if (favList == null) {
                    myView.visibility = View.GONE
                    return
                }
                val favAdapter =
                    CustomAdapterFavorite(
                        position,
                        this,
                        favList as ArrayList<Selected>,
                        dataBaseAdapter
                    )
                recyclerView.adapter = favAdapter
            }

            2 -> {
                val favList =
                    dataBaseAdapter.getSelectedPages()
                        ?.filter { it1 -> it1.favorite == 0 && it1.description.isEmpty() }
                if (favList == null) {
                    myView.visibility = View.GONE
                    return
                }
                val favAdapter =
                    CustomAdapterSelected(
                        position,
                        this,
                        favList as ArrayList<Selected>,
                        dataBaseAdapter
                    )
                recyclerView.adapter = favAdapter
            }

            3 -> {
                val favList = dataBaseAdapter.getSelectedPages()
                    ?.filter { it1 -> it1.favorite == 0 && it1.description.isNotEmpty() }
                if (favList == null) {
                    myView.visibility = View.GONE
                    return
                }
                val favAdapter =
                    CustomAdapterCommented(
                        requireActivity(),
                        position,
                        this,
                        favList as ArrayList<Selected>,
                        dataBaseAdapter
                    )
                recyclerView.adapter = favAdapter
            }
        }

    }

    fun update(position: Int) {
        createUpdateMyView(position, DataFromFile2.dataBaseAdapter)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        myView = view
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.itemAnimator = DefaultItemAnimator()

        pageViewModel.whatTabs.observe(viewLifecycleOwner, Observer {
            update(it)
        })
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.recycler_chapter, container, false)
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): ChaptersFragment {
            return ChaptersFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }
}