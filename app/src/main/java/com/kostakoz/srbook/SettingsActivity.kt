package com.kostakoz.srbook


import android.app.Dialog
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.*
import kotlinx.android.synthetic.main.settings_activity.*
import org.jsoup.Jsoup

private const val TAG = "mySettings"

class SettingsActivity : AppCompatActivity() {

    private var oldTextSize = 0
    private var newTextSize = 0

    private var oldLineSpace = 0
    private var newLineSpace = 0

    private var oldLeftRightMargins = 0
    private var newLeftRightMargins = 0

    private var currentTypeFace = ""
    private var currentTextSize = 0
    private var currentLineSpace = 0
    private var currentLeftRightMargins = 0

    private val orientationScreenList = listOf("Автоматический", "Вертикальный", "Горизонтальный")
    private val orientationSwipePageList = listOf("Горизонтальный", "Вертикальный")

    private var textColor = "black"

    private val percent = listOf(
        "0 %",
        "10 %",
        "20 %",
        "30 %",
        "40 %",
        "50 %",
        "60 %",
        "70 %",
        "80 %",
        "90 %",
        "100 %"
    )

    private val percentLineSpace = listOf(
        "0 %",
        "10 %",
        "20 %",
        "30 %",
        "40 %",
        "50 %",
        "60 %",
        "70 %",
        "80 %",
        "90 %",
        "100 %",
        "110 %"
    )

    private val percentLeftRightMargins = listOf(
        "0 %",
        "10 %",
        "20 %",
        "30 %",
        "40 %",
        "50 %",
        "60 %",
        "70 %",
        "80 %",
        "90 %",
        "100 %",
        "110 %"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        MyPref.themeAndOrientationConfig(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = DataFromFile2.dataBaseAdapter.getTitle()
        toolbar.title = DataFromFile2.dataBaseAdapter.getTitle()
        if (MyPref.getThemeMode() == 2) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
            toolbar.setTitleTextColor(resources.getColor(R.color.textColorPrimaryNight))
        }else{
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp2)
        }
        toolbar.setNavigationOnClickListener {
            goBack()
        }

        when (MyPref.getThemeMode()) {
            2 -> textColor = "white"
        }

        webView.setBackgroundColor(Color.TRANSPARENT)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            btnModeDay.setBackgroundColor(Color.GRAY)
            btnModeSepiy.setBackgroundColor(resources.getColor(R.color.colorBackgroundSepiy))
            btnModeNight.setBackgroundColor(resources.getColor(R.color.colorBackgroundNight))
            when (MyPref.getThemeMode()) {
                0 -> btnModeDay.setBackgroundResource(R.drawable.btn_day_active)
                1 -> btnModeSepiy.setBackgroundResource(R.drawable.btn_sepiy_active)
                2 -> btnModeNight.setBackgroundResource(R.drawable.btn_night_active)
                else -> btnModeDay.setBackgroundResource(R.drawable.btn_day_active)
            }
        }

        currentLineSpace = MyPref.getLineSpace()
        oldLineSpace = MyPref.getLineSpace()
        newLineSpace = MyPref.getLineSpace()

        currentTextSize = MyPref.getTextSize()
        oldTextSize = MyPref.getTextSize()
        newTextSize = MyPref.getTextSize()

        currentLeftRightMargins = MyPref.getRightLeftMargins()
        oldLeftRightMargins = MyPref.getRightLeftMargins()
        newLeftRightMargins = MyPref.getRightLeftMargins()

        currentTypeFace = TypeFace.typefaceList[MyPref.getTypeFace()].name

        typeFaces()
        orientationScreen()
        orientationSwipePage()
        setTextSize()
        setThemeMode()
        setLineSpace()
        leftRightMargins()
        when (MyPref.getThemeMode()) {
            2 -> {
                spinner_change_swipe_page_animation.setPopupBackgroundResource(R.color.colorPrimaryNight)
                spinner_change_screen_orientation.setPopupBackgroundResource(R.color.colorPrimaryNight)
                spinner_change_font_family.setPopupBackgroundResource(R.color.colorPrimaryNight)
            }
        }
    }

    override fun onBackPressed() {
        goBack()
    }

    private fun goBack() {
        if (oldTextSize != newTextSize
            || oldLineSpace != newLineSpace
            || oldLeftRightMargins != newLeftRightMargins
        ) {
            DataFromFile2.dataBaseAdapter.setAfterCountPages(0)
            DataFromFile2.dataBaseAdapter.setCountPages(0)
            DataFromFile2.isChangeTextSize = true
        }

        showLoaderDialog()

        val intent = Intent(
            this,
            FullscreenActivity::class.java
        ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(intent)
        finish()
    }

    private fun showLoaderDialog() {
        val settingsDialog = Dialog(
            this,
            android.R.style.Theme_Light_NoTitleBar_Fullscreen
        )
        settingsDialog.window?.requestFeature(Window.FEATURE_NO_TITLE)
        settingsDialog.setContentView(
            layoutInflater.inflate(
                R.layout.loader
                , null
            )
        )
        val rLayout = settingsDialog.findViewById<RelativeLayout>(R.id.rlayout)
        val textView = settingsDialog.findViewById<TextView>(R.id.textView)
        when (MyPref.getThemeMode()) {
            1 -> {
                rLayout.setBackgroundResource(R.color.colorBackgroundSepiy)
                textView.setTextColor(resources.getColor(R.color.textColorPrimarySepiy))
            }
            2 -> {
                rLayout.setBackgroundResource(R.color.colorBackgroundNight)
                textView.setTextColor(resources.getColor(R.color.textColorPrimaryNight))
            }
        }
        settingsDialog.show()
    }

    private fun loadWebView() {
        val links = """
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        """.trimIndent()

        val htmlText = resources.getString(R.string.lorem_text_for_example)
        val html = Jsoup.parse(htmlText)
        val style = """
                    <style id="styleId">
                    body{
                        text-align: justify;
                        font-family: '$currentTypeFace';
                        line-height: ${LineSpace.lineSpaceListWeb[currentLineSpace]};
                    }
                    p{
                        font-size: ${TextSize.textSizeListWeb[currentTextSize]}px;
                        color: $textColor;
                    }
                    ${TypeFace.fontFaceStyle}
                    </style>
                """.trimIndent()
        html.head().append(links)
        html.body().append(style)
        Log.d(TAG, html.html())
        webView.post {
            webView.loadDataWithBaseURL(
                "file:///android_asset/",
                html.html(),
                "text/html",
                "utf-8",
                null
            )
        }
    }

    private fun refreshActivity() {
        if (oldTextSize != newTextSize
            || oldLineSpace != newLineSpace
            || oldLeftRightMargins != newLeftRightMargins
        ) {
            DataFromFile2.dataBaseAdapter.setAfterCountPages(0)
            DataFromFile2.dataBaseAdapter.setCountPages(0)
            DataFromFile2.isChangeTextSize = true
        }
        finish()
        startActivity(intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP))
        overridePendingTransition(0, 0)
    }

    private fun setThemeMode() {

        btnModeDay.setOnClickListener {
            if (MyPref.getThemeMode() != 0) {
                MyPref.setThemeMode(0)
                refreshActivity()
            }
        }
        btnModeSepiy.setOnClickListener {
            if (MyPref.getThemeMode() != 1) {
                MyPref.setThemeMode(1)
                refreshActivity()
            }
        }
        btnModeNight.setOnClickListener {
            if (MyPref.getThemeMode() != 2) {
                MyPref.setThemeMode(2)
                refreshActivity()
            }
        }
    }

    private fun setTextSize() {
        val maxTextSize = TextSize.textSizeListWeb.size - 1
        val minTextSize = 0

        label_persent_text_size.text = percent[currentTextSize]

        btnTextSizePlus.setOnClickListener {
            DataFromFile2.isPlus = true
            currentTextSize = if (currentTextSize >= maxTextSize) maxTextSize else ++currentTextSize
            label_persent_text_size.text = percent[currentTextSize]

            MyPref.setTextSize(currentTextSize)

            newTextSize = currentTextSize

            loadWebView()

        }

        btnTextSizeMinus.setOnClickListener {
            DataFromFile2.isPlus = false
            currentTextSize = if (currentTextSize <= minTextSize) minTextSize else --currentTextSize
            label_persent_text_size.text = percent[currentTextSize]

            MyPref.setTextSize(currentTextSize)

            newTextSize = currentTextSize

            loadWebView()
        }
    }

    private fun setLineSpace() {
        val maxLineSpace = LineSpace.lineSpaceList.size.minus(1)
        val minLineSpace = 0

        label_persent_line_space.text = percentLineSpace[currentLineSpace]

        btnChangeLineSpacePlus.setOnClickListener {
            DataFromFile2.isPlus = true
            currentLineSpace =
                if (currentLineSpace >= maxLineSpace) maxLineSpace else ++currentLineSpace
            label_persent_line_space.text = percentLineSpace[currentLineSpace]

            MyPref.setLineSpace(currentLineSpace)

            newLineSpace = currentLineSpace

            loadWebView()
        }

        btnChangeLineSpaceMinus.setOnClickListener {
//            DataFromFile2.isPlus = false
            currentLineSpace =
                if (currentLineSpace <= minLineSpace) minLineSpace else --currentLineSpace
            label_persent_line_space.text = percentLineSpace[currentLineSpace]

            MyPref.setLineSpace(currentLineSpace)

            newLineSpace = currentLineSpace

            loadWebView()
        }
    }

    private fun orientationSwipePage() {
        val arrayAdapter =
            ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                orientationSwipePageList
            )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_change_swipe_page_animation.adapter = arrayAdapter

        spinner_change_swipe_page_animation.setSelection(MyPref.getAnimationSwipePage())
        spinner_change_swipe_page_animation.onItemSelectedListener =
            object : OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    MyPref.setAnimationSwipePage(position)

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    Toast.makeText(this@SettingsActivity, "Nothing select", Toast.LENGTH_SHORT)
                        .show()
                }
            }
    }

    private fun orientationScreen() {
        val arrayAdapter =
            ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                orientationScreenList
            )
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_change_screen_orientation.adapter = arrayAdapter
        spinner_change_screen_orientation.setSelection(MyPref.getOrientationScreen())
        spinner_change_screen_orientation.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                MyPref.setOrientationScreen(position)
                requestedOrientation = when (position) {
                    0 -> ActivityInfo.SCREEN_ORIENTATION_SENSOR
                    1 -> ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                    2 -> ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                    else -> ActivityInfo.SCREEN_ORIENTATION_SENSOR
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@SettingsActivity, "Nothing select", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun typeFaces() {
        val fontNames = ArrayList<String>()
        for (typeface in TypeFace.typefaceList) {
            fontNames.add(typeface.name)
        }

        val arrayAdapterTypeFace =
            ArrayAdapter(this, android.R.layout.simple_spinner_item, fontNames)
        arrayAdapterTypeFace.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spinner_change_font_family.adapter = arrayAdapterTypeFace
        spinner_change_font_family.setSelection(MyPref.getTypeFace())
        spinner_change_font_family.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val typeface = TypeFace.typefaceList.first { it.id == position }
                Toast.makeText(
                    this@SettingsActivity,
                    typeface.name, Toast.LENGTH_SHORT
                ).show()

                MyPref.setTypeFace(position)

                currentTypeFace = typeface.name

                loadWebView()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@SettingsActivity, "Nothing select", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun leftRightMargins() {
        val maxLeftRightMargins = LeftRightMargins.leftRightMarginsList.size.minus(1)
        val minLeftRightMargins = 0
        var margins = (webView.layoutParams as ViewGroup.MarginLayoutParams).apply {
            leftMargin = LeftRightMargins.leftRightMarginsList[currentLeftRightMargins]
            rightMargin = LeftRightMargins.leftRightMarginsList[currentLeftRightMargins]
        }
        webView.layoutParams = margins
        label_persent_margin.text = percentLeftRightMargins[currentLeftRightMargins]

        btnChangeMarginPlus.setOnClickListener {
            currentLeftRightMargins =
                if (currentLeftRightMargins >= maxLeftRightMargins) maxLeftRightMargins else ++currentLeftRightMargins
            label_persent_margin.text = percentLeftRightMargins[currentLeftRightMargins]
            MyPref.setRightLeftMargins(currentLeftRightMargins)
            newLeftRightMargins = currentLeftRightMargins
            margins = (webView.layoutParams as ViewGroup.MarginLayoutParams).apply {
                leftMargin = LeftRightMargins.leftRightMarginsList[currentLeftRightMargins]
                rightMargin = LeftRightMargins.leftRightMarginsList[currentLeftRightMargins]
            }
            webView.layoutParams = margins
        }

        btnChangeMarginMinus.setOnClickListener {
            currentLeftRightMargins =
                if (currentLeftRightMargins <= minLeftRightMargins) minLeftRightMargins else --currentLeftRightMargins
            label_persent_margin.text = percentLeftRightMargins[currentLeftRightMargins]
            MyPref.setRightLeftMargins(currentLeftRightMargins)
            newLeftRightMargins = currentLeftRightMargins
            margins = (webView.layoutParams as ViewGroup.MarginLayoutParams).apply {
                leftMargin = LeftRightMargins.leftRightMarginsList[currentLeftRightMargins]
                rightMargin = LeftRightMargins.leftRightMarginsList[currentLeftRightMargins]
            }
            webView.layoutParams = margins
        }

    }


}

