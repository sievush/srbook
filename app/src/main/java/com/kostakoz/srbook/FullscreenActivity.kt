package com.kostakoz.srbook

import android.annotation.SuppressLint
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.TextPaint
import android.util.Log
import android.view.*
import android.widget.SeekBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.google.android.gms.ads.*
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.*
import com.kostakoz.srbook.srmvc.model.dataclasses.Chapters
import com.kostakoz.srbook.srmvc.model.dataclasses.Pages
import kotlinx.android.synthetic.main.activity_fullscreen.*


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
private const val FULL_ACTIVITY_TAG = "myfulltag"

class FullscreenActivity : AppCompatActivity(), SeekBar.OnSeekBarChangeListener {
    private var isOnCreate = false
    private val mHideHandler = Handler()

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private val mHidePart2Runnable = Runnable {
        // Delayed removal of status and navigation bar

        // Note that some of these constants are new as of API 16 (Jelly Bean)
        // and API 19 (KitKat). It is safe to use them, as they are inlined
        // at compile-time and do nothing on earlier devices.
        newPager.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LOW_PROFILE or
                    View.SYSTEM_UI_FLAG_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
    }

    private val mShowPart2Runnable = Runnable {
        // Delayed display of UI elements
        supportActionBar?.show()
        fullscreen_content_controls.visibility = View.VISIBLE
    }
    var mVisible: Boolean = false
    private val mHideRunnable = Runnable { hide() }

    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private val mDelayHideTouchListener = View.OnTouchListener { _, _ ->
        if (AUTO_HIDE) {
            delayedHide(AUTO_HIDE_DELAY_MILLIS)
        }
        false
    }
    val dataBaseAdapter = DataFromFile2.dataBaseAdapter

    private fun getBottomNavigationSizeInPixels(): Int {
        val resourceId: Int = resources.getIdentifier("navigation_bar_height", "dimen", "android")
        return if (resourceId > 0) {
            resources.getDimensionPixelSize(resourceId)
        } else 0
    }

    override fun onStart() {
        super.onStart()
        if (!isOnCreate) {
            newPager.setCurrentItem(dataBaseAdapter.getCurrentPage(), false)
        } else {
            isOnCreate = false
        }
        Log.d(FULL_ACTIVITY_TAG, "onStart()")
    }

    override fun onResume() {
        super.onResume()
        Log.d(FULL_ACTIVITY_TAG, "onResume()")
    }

    lateinit var pages: Pages
    lateinit var chapters: Chapters
    private lateinit var mInterstitialAd: InterstitialAd


    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        MyPref.themeAndOrientationConfig(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen)
//        hide()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        if (MyPref.getThemeMode() == 2) {
            toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp)
        } else {
            toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp2)
        }
        toolbar.title = ""

        setSupportActionBar(toolbar)

        MobileAds.initialize(this) {}
        MobileAds.setRequestConfiguration(
            RequestConfiguration.Builder()
                .setTestDeviceIds(listOf("samsung-sm_n950f-ce07171722d0b41c027e"))
                .build()
        )

        mInterstitialAd = InterstitialAd(this).apply {
            adUnitId = "ca-app-pub-3940256099942544/1033173712"
            adListener = (object : AdListener() {
                override fun onAdLoaded() {
                    Toast.makeText(this@FullscreenActivity, "onAdLoaded()", Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    Toast.makeText(
                        this@FullscreenActivity,
                        "onAdFailedToLoad() with error code: $errorCode",
                        Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onAdClosed() {

                }
            })
        }

//        mInterstitialAd = InterstitialAd(this)
//        mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"
        mInterstitialAd.loadAd(AdRequest.Builder().build())

        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdClosed() {
                mInterstitialAd.loadAd(AdRequest.Builder().build())
//                toggle()
            }
        }



        Log.d(FULL_ACTIVITY_TAG, "onCreate()")
        isOnCreate = true

        mVisible = true

        val textPaint = TextPaint()
        textPaint.textSize = TextSize.textSizeList[MyPref.getTextSize()]

        newPager.viewTreeObserver.addOnGlobalLayoutListener(
            object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    newPager.viewTreeObserver.removeOnGlobalLayoutListener(this)

                    //320
                    val newWidth = newPager.width - (LeftRightMargins.leftRightMarginsList[MyPref.getRightLeftMargins()] * 2)
                    Log.d("checkMargins", newWidth.toString())
                    when (MyPref.getAnimationSwipePage()) {
                        0 -> newPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
                        1 -> newPager.orientation = ViewPager2.ORIENTATION_VERTICAL
                        else -> newPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
                    }
                    if (dataBaseAdapter.getCountPages() == 0 || (dataBaseAdapter.getCountPages() != dataBaseAdapter.getAfterCountPages())) {
                        dataBaseAdapter.deletePages()
                        DataFromFile2.doPagesForViewPager2(
                            textPaint,
                            newWidth,
                            newPager.height// - 100// - getBottomNavigationSizeInPixels() - 100
                        )
                        dataBaseAdapter.setCountPages(DataFromFile2.pages!!.size)
                        MyTask().execute()
                    } else {
                        DataFromFile2.pages = dataBaseAdapter.getPages()!!
                        DataFromFile2.elsePages = dataBaseAdapter.getElsePages()!!
                    }
                    val pageAdapter =
                        PageAdapter(
                            supportFragmentManager, lifecycle,
                            DataFromFile2.pages!!.size
                        )
                    newPager.adapter = pageAdapter
                    newPager.setCurrentItem(dataBaseAdapter.getCurrentPage(), false)


                    seekBar.max = dataBaseAdapter.getCountPages()
                    seekBar.progress = dataBaseAdapter.getCurrentPage() + 1


                }
            }
        )


        newPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            @SuppressLint("SetTextI18n")
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                pages = DataFromFile2.pages!!.first { it.page == position }
                DataFromFile2.startPosition = pages.start
                DataFromFile2.endPosition = pages.end
                DataFromFile2.startPositionId = pages.id_data
                DataFromFile2.currentPageText = pages.text
                dataBaseAdapter.updateCurrentPage(position)
                changeFavoriteIcon(position)
                seekBar.progress = position + 1
                chapters = dataBaseAdapter.getOneChapters(pages.id_data)!!
                chapterName.text = chapters.title
                pageName.text = """${position + 1} из ${dataBaseAdapter.getCountPages()}"""
                val pageCount = DataFromFile2.elsePages[pages.id_data - 1]
                val getPageCount = pageCount - position
                elseMorePage.text = if (getPageCount > 0) "Еще $getPageCount стр." else ""
                val showIfYouCan = 5 % (position + 1) == 0
                if (mInterstitialAd.isLoaded && showIfYouCan) {
//                    mInterstitialAd.show()
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.")
                }

//                var i = 0
//                supportFragmentManager.fragments.forEach {
//                    supportFragmentManager.beginTransaction().add(it, "$i")
//                    supportFragmentManager.beginTransaction().detach(it)
//                    supportFragmentManager.beginTransaction().attach(it)
//                    supportFragmentManager.beginTransaction().commit()
//                    i++
//                    Log.d("fragtag", it.tag)
//                }
            }
        })

//        seekBar.setPadding(0, 0, 0, 0)

        seekBar.setOnSeekBarChangeListener(this)

        when (MyPref.getThemeMode()) {
            2 -> fullscreen_content_controls.setBackgroundResource(R.color.colorPrimaryNight)
            else -> fullscreen_content_controls.setBackgroundResource(R.color.colorPrimary)
        }


    }

    @SuppressLint("StaticFieldLeak")
    inner class MyTask : AsyncTask<Void, Void, Void>() {
        override fun doInBackground(vararg params: Void?): Void? {
            this@FullscreenActivity.runOnUiThread {
                Toast.makeText(
                    MainApplication.applicationContext(),
                    "Start add to database!",
                    Toast.LENGTH_LONG
                ).show()
            }
//            dataBaseAdapter.createDatabase()
//            dataBaseAdapter.open()
            if (DataFromFile2.pages!!.size > 0) {
                dataBaseAdapter.deleteElsePages()
                DataFromFile2.elsePages.forEach {
                    dataBaseAdapter.setElsePages(it)
                }
                for (page in DataFromFile2.pages!!) {
                    dataBaseAdapter.setPages(
                        id = page.id,
                        id_data = page.id_data,
                        pages = page.pages,
                        start = page.start,
                        end = page.end,
                        page = page.page,
                        text = page.text
                    )
                }
                dataBaseAdapter.setAfterCountPages(DataFromFile2.pages!!.size)
                this@FullscreenActivity.runOnUiThread {
                    Toast.makeText(
                        MainApplication.applicationContext(),
                        "Success add to database!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }

            return null
        }

    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(1) //100
    }

    fun toggle() {
        Log.d("myFragment", mVisible.toString())
        if (mVisible) {
            hide()
            Log.d("myFragment", "hide()")
        } else {
            show()
            Log.d("myFragment", "show()")
        }
    }

    private fun hide() {
        // Hide UI first
        supportActionBar?.hide()
        fullscreen_content_controls.visibility = View.GONE
        mVisible = false

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY.toLong())
        }
    }

    //newPager.systemUiVisibility =
    //            View.SYSTEM_UI_FLAG_LOW_PROFILE or
    //                    View.SYSTEM_UI_FLAG_FULLSCREEN or
    //                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
    //                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or
    //                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or
    //                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION

    private fun show() {
        // Show the system bar
        newPager.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        mVisible = true

        // Schedule a runnable to display UI elements after a delay
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mHideHandler.removeCallbacks(mHidePart2Runnable)
        }
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY.toLong())
    }

    /**
     * Schedules a call to hide() in [delayMillis], canceling any
     * previously scheduled calls.
     */
    private fun delayedHide(delayMillis: Int) {
        mHideHandler.removeCallbacks(mHideRunnable)
        mHideHandler.postDelayed(mHideRunnable, delayMillis.toLong())
    }

    companion object {
        /**
         * Whether or not the system UI should be auto-hidden after
         * [AUTO_HIDE_DELAY_MILLIS] milliseconds.
         */
        private val AUTO_HIDE = true

        /**
         * If [AUTO_HIDE] is set, the number of milliseconds to wait after
         * user interaction before hiding the system UI.
         */
        private val AUTO_HIDE_DELAY_MILLIS = 1 //3000

        /**
         * Some older devices needs a small delay between UI widget updates
         * and a change of the status and navigation bar.
         */
        private val UI_ANIMATION_DELAY = 1 //300

    }

    private lateinit var menu: Menu

    private fun changeFavoriteIcon(page: Int) {
        if (this::menu.isInitialized) {
            val menuItem = this.menu.findItem(R.id.favoriteMenu)!!
            val selected =
                DataFromFile2.dataBaseAdapter.getOneSelectedPages(page)
            if (selected != null) {
                menuItem.icon = ContextCompat.getDrawable(
                    this,
                    if (MyPref.getThemeMode() == 2) {
                        R.drawable.ic_bookmark_black_24dp
                    } else {
                        R.drawable.ic_bookmark_black_24dp2
                    }
                )
            } else {
                menuItem.icon = ContextCompat.getDrawable(
                    this,
                    if (MyPref.getThemeMode() == 2) {
                        R.drawable.ic_bookmark_border_black_24dp
                    } else {
                        R.drawable.ic_bookmark_border_black_24dp2
                    }
                )
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.chapters_menu, menu)
        this.menu = menu!!
        changeFavoriteIcon(DataFromFile2.dataBaseAdapter.getCurrentPage())

        if (MyPref.getThemeMode() == 2) {
            menu.findItem(R.id.settingsMenu).icon =
                ContextCompat.getDrawable(this, R.drawable.ic_settings_black_24dp)
            menu.findItem(R.id.searchMenu).icon =
                ContextCompat.getDrawable(this, R.drawable.ic_search_black_24dp)
            menu.findItem(R.id.infoMenu).icon =
                ContextCompat.getDrawable(this, R.drawable.ic_info_outline_black_24dp)
            menu.findItem(R.id.refreshMenu).icon =
                ContextCompat.getDrawable(this, R.drawable.ic_refresh_black_24dp)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.favoriteMenu -> {
                Log.d(FULL_ACTIVITY_TAG, "favoriteMenu")
                val currentPage = dataBaseAdapter.getCurrentPage()
                val selected =
                    dataBaseAdapter.getOneSelectedPages(currentPage)
                if (selected == null) {
                    val pages = dataBaseAdapter.getOnePage(currentPage)!!
                    dataBaseAdapter.setSelectedPages(
                        1,
                        pages.id_data,
                        pages.start,
                        pages.end,
                        pages.start,
                        pages.end,
                        pages.page,
                        0,
                        pages.text,
                        ""
                    )
                } else {
                    dataBaseAdapter.deleteSelectedPages(selected.id)
                }

                changeFavoriteIcon(currentPage)

            }
            R.id.settingsMenu -> {
                Log.d(FULL_ACTIVITY_TAG, "settingsMenu")
                startActivity(
                    Intent(
                        this,
                        SettingsActivity::class.java
                    )
                )
            }
            R.id.searchMenu -> {
                Log.d(FULL_ACTIVITY_TAG, "searchMenu")
                startActivity(
                    Intent(
                        this,
                        SearchActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                )
                finish()
            }
            android.R.id.home -> {
                Log.d(FULL_ACTIVITY_TAG, "home")
                startActivity(Intent(this, ChaptersActivity::class.java))
//                finish()
            }
            R.id.infoMenu -> {
                startActivity(Intent(this, InfoActivity::class.java))
            }
            R.id.refreshMenu -> {
                val tagF = "f${DataFromFile2.dataBaseAdapter.getCurrentPage()}"
                val fragment: MyFragment = DataFromFile2.fragmentMap[tagF] as MyFragment

                Log.d("taglog2", fragment.tag!!)
                val dataBaseAdapter = DataFromFile2.dataBaseAdapter
                val position = dataBaseAdapter.getCurrentPage()
                val currentHtmlPage =
                    dataBaseAdapter.getOnePage(position) ?: DataFromFile2.pages?.get(position) ?: return true
                val myHtmlString = currentHtmlPage.pages
                fragment.loadWeb(myHtmlString)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {

    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        newPager.setCurrentItem(seekBar!!.progress, false)
    }
}