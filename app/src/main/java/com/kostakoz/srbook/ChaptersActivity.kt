package com.kostakoz.srbook

import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.View
import com.google.android.material.tabs.TabLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.text.bold
import androidx.core.text.color
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import com.kostakoz.srbook.ui.main.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_chapters.*

class ChaptersActivity : AppCompatActivity() {
    var isChangeMainProperties = false;
    override fun onCreate(savedInstanceState: Bundle?) {
        MyPref.themeAndOrientationConfig(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chapters)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = DataFromFile2.dataBaseAdapter.getTitle()
        toolbar.title = DataFromFile2.dataBaseAdapter.getTitle()
        if (MyPref.getThemeMode() == 2) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
            toolbar.setTitleTextColor(resources.getColor(R.color.textColorPrimaryNight))
        }else{
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp2)
        }
        toolbar.setNavigationOnClickListener {
            goBack()
        }

        val sectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager, lifecycle)
        val viewPager: ViewPager2 = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)

        val nameOfTab = listOf<CharSequence>("ОГЛАВЛЕНИЕ", "ЗАКЛАДКИ", "СОХРАНЕННЫЕ", "КОММЕНТАРИИ")
        val icons = listOf(
            R.drawable.ic_menu_black_24dp2,
            R.drawable.ic_bookmark_black_24dp2,
            R.drawable.ic_save_black_24dp,
            R.drawable.ic_comment_black_24dp
        )
        TabLayoutMediator(tabs, viewPager) { tab, position ->
            val bold = SpannableStringBuilder().bold { append(nameOfTab[position]) }

            tab.text = bold
            tab.icon = resources.getDrawable(icons[position])
        }.attach()


    }

    override fun onBackPressed() {
        goBack()
    }

    private fun goBack() {
        progressBar.visibility = View.VISIBLE
        if (isChangeMainProperties) {
            startActivity(
                Intent(
                    this,
                    FullscreenActivity::class.java
                ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            )
            finish()
        } else finish()
    }
}