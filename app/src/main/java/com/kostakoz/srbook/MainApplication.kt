package com.kostakoz.srbook

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.kostakoz.srbook.preferences.MyPref
import com.kostakoz.srbook.srmvc.model.DataFromFile2
import com.kostakoz.srbook.srmvc.model.savedata.DataBaseAdapter

class MainApplication : MultiDexApplication() {

    init {
        instance = this
    }

    init {
        filesDir.delete().and(obbDir.exists())
        
    }
    companion object {
        private var instance: MainApplication? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
//        deleteDatabase("properties.db")
//        MyPref.clearAll()
        DataFromFile2.dataBaseAdapter = DataBaseAdapter()
        DataFromFile2.dataBaseAdapter.createDatabase()
        DataFromFile2.dataBaseAdapter.open()
    }
}

//Smart cast to 'kotlin.collections.ArrayList<Pages> /* = java.util.ArrayList<Pages> */' is impossible, because 'DataFromFile2.pages' is a mutable property that could have been changed by this time