 $(document).ready(function(){

     if (!window.x) {
         x = {};
     }

     x.Selector = {};
     x.Selector.getSelected = function() {
         var t = '';
         if (window.getSelection) {
             t = window.getSelection();
         } else if (document.getSelection) {
             t = document.getSelection();
         } else if (document.selection) {
             t = document.selection.createRange().text;
         }
         return t;
     }

    function getSelectionCharacterOffsetsWithin(element) {
        var startOffset = 0, endOffset = 0;
        if (typeof window.getSelection != "undefined") {
            var range = window.getSelection().getRangeAt(0);
            var coords = range.getBoundingClientRect();
            var preCaretRange = range.cloneRange();
            preCaretRange.selectNodeContents(element);
            preCaretRange.setEnd(range.startContainer, range.startOffset);
            startOffset = preCaretRange.toString().length;
            endOffset = startOffset + range.toString().length;
        } else if (typeof document.selection != "undefined" &&
                   document.selection.type != "Control") {
            var textRange = document.selection.createRange();
            var preCaretTextRange = document.body.createTextRange();
            preCaretTextRange.moveToElementText(element);
            preCaretTextRange.setEndPoint("EndToStart", textRange);
            startOffset = preCaretTextRange.text.length;
            endOffset = startOffset + textRange.text.length;
        }
        return { start: startOffset, end: endOffset, bottom: coords.bottom };
    }

$(document).on('selectionchange', function(e) {
    var selectedText = x.Selector.getSelected();
    if(selectedText != ''){
        var selOffsets = getSelectionCharacterOffsetsWithin(document.getElementsByTagName("body")[0]);
           $('.custom-menu li').attr("selectedText", selectedText)
           $('.custom-menu li').attr("startpos", selOffsets.start)
           $('.custom-menu li').attr("endpos", selOffsets.end)
           $('.custom-menu').center(selOffsets.bottom);
           $('.custom-menu').show();
    }
});


      $("body").bind("touchstart touchend contextmenu", function(e) {

            var selectedText = x.Selector.getSelected();

             if(e.type == "contextmenu"){
                e.preventDefault();
             }else if(e.type == "touchend"){
                 if(selectedText != ''){
                    if ($(e.target).parents(".custom-menu").length > 0) {
                        $(".custom-menu").hide(100);
                    }
                 }else{
                    Android.showToast("unselected");
                    $(".custom-menu").hide(100);
                 }
              }else if(e.type == "touchstart"){
                // If the clicked element is not the menu
                    if (!$(e.target).parents(".custom-menu").length > 0) {
                        $(".custom-menu").hide(100);
                    }
              }
         });

         $("p img").on("click", function(){
            Android.imgClick($(this).attr('src'));
         });

        jQuery.fn.center = function (thisY) {
            this.css("position","absolute");
            var winHeight = $(window).height();
            var thisHeight = $(this).outerHeight();
            var winWidth = $(window).width();
            var thisWidth = $(this).outerWidth();
            var winScrollTop = $(window).scrollTop();
            var winScrollLeft = $(window).scrollLeft();
            if(thisY < 100){
                this.css("top", (thisY + 50) + "px");
            }else{
                this.css("top", (thisY - thisHeight - 50) + "px"); // чапашба кадан даркор =)
            }

            this.css("left", Math.max(0, ((winWidth - thisWidth) / 2) +
                                                        winScrollLeft) + "px");
            return this;
        }


        $(".custom-menu li").bind("click", function(event){

            // This is the triggered action name
            switch($(this).attr("data-action")) {

                // A case for each action. Your actions here
                case "pallitre":

                break;
                case "save":
                Android.selectedTextMenu("save", $(this).attr("startpos"), $(this).attr("endpos"), $(this).attr("selectedtext"));
                break;
                case "comment":
                Android.selectedTextMenu("comment", $(this).attr("startpos"), $(this).attr("endpos"), $(this).attr("selectedtext"));
                break;
                case "copy":
                Android.selectedTextMenu("copy", $(this).attr("startpos"), $(this).attr("endpos"), $(this).attr("selectedtext"));
                break;
                case "share":
                 Android.selectedTextMenu("share", $(this).attr("startpos"), $(this).attr("endpos"), $(this).attr("selectedtext"));
                 break;
                  default: $(".custom-menu").hide(100); break;

            }

            // Hide it AFTER the action was triggered
            $(".custom-menu").hide(100);

            return false;
        });

});

